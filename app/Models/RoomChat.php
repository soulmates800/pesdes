<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoomChat extends Model
{
    protected $table = 'chat_room';

    protected $primaryKey = 'id';
	
	protected $guarded = [];
}
