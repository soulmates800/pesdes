<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\AuthControllers as Controller;
use Illuminate\Http\Request;

class LoginControllers extends Controller
{
    public function login()
    {
        return view('Main.login');
    }

    public function register()
    {
        return view('Main.register');
    }
}
