<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller as Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileControllers extends Controller
{
    public function index()
    {
        $data['user_data'] = Auth::user();
        return view('User.profile',compact('data'));
    }
}
