<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller as Controller;
use App\Models\Surat;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeControllers extends Controller
{
    public function index()
    {
        $data['user_data'] = Auth::user();
        return view('User.dashboard',compact('data'));
    }

    public function adm_index()
    {
        $dSemua = Surat::get();
        $dDirposes = Surat::where('status', 'Diproses')->get();
        $dDiterima = Surat::where('status', 'Diterima')->get();
        $dDitolak = Surat::where('status', 'Ditolak')->get();
        $data['semua'] = count($dSemua);
        $data['diproses'] = count($dDirposes);
        $data['diterima'] = count($dDiterima);
        $data['ditolak'] = count($dDitolak);

        $data['surat'] = Surat::orderBy('updated_at', 'desc')->limit(3)->get();
        $data['user_aktivity'] = User::orderBy('created_at', 'desc')->limit(3)->get();

        $data['user_data'] = Auth::user();

        $counts = count($data['surat']);
        for ($i=0; $i < $counts; $i++) {
            $data['user'][$i] = User::where('id', $data['surat'][$i]->user_id)->limit(3)->first();
        }
        
        return view('Admin.dashboard',compact('data'));
    }
}
