<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller as Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserControllers extends Controller
{
    public function adm_daftar_user()
    {
        $data['user_data'] = Auth::user();
        return view('Admin.userlist',compact('data'));
    }

    public function adm_tambah_user()
    {
        $data['user_data'] = Auth::user();
        return view('Admin.adduser',compact('data'));
    }
    public function adm_profile()
    {
        $data['user_data'] = Auth::user();
        return view('Admin.profile',compact('data'));
    }
}
