<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller as Controller;
use App\Models\Surat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SuratControllers extends Controller
{
    public function index()
    {
        $data['user_data'] = Auth::user();
        return view('User.surat',compact('data'));
    }

    public function detail($kode)
    {
        $data['user_data'] = Auth::user();
        $data["surat"] = Surat::where('kode_surat', $kode)->first();
        return view('User.detail',compact('data'));
    }

    public function adm_index()
    {
        $data['user_data'] = Auth::user();
        return view('Admin.surat',compact('data'));
    }

    public function update_surat()
    {
        $data['user_data'] = Auth::user();
        return view('User.edit_surat',compact('data'));
    }
}
