<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Support\Facades\Auth;

class ChatControllers extends Controller
{
    public function adm_index()
    {
        $data['user_data'] = Auth::user();
        return view('Admin.customer_service',compact('data'));
    }

    public function adm_detail_chat()
    {
        $data['user_data'] = Auth::user();
        return view('Admin.detail_customer_service',compact('data'));
    }
}
