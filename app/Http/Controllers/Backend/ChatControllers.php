<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller as Controller;
use App\Models\Chat;
use App\Models\RoomChat;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatControllers extends Controller
{

    public function get_pesan_chat()
    {
        $query['chat'] = Chat::where([['id_penerima', Auth::user()->id], ['nama_penerima', Auth::user()->name]])->orWhere([['id_pengirim', Auth::user()->id], ['nama_pengirim', Auth::user()->name]])->get();
        $query['user'] = Auth::user();
        if ($query) {
            return response()->json([
                '_status' => 200,
                '_data' => $query,
            ]);
            }
        return 'error';
    }

    public function kirim_chat(Request $request)
    {

        $getRoom = RoomChat::where('user_id', Auth::user()->id)->first();

        if ($getRoom == null) {
            RoomChat::create([
                'uniqid' => $request->uniqid,
                'user_id' => Auth::user()->id,
                'nama_user' => Auth::user()->name,
                'status' => 'New',
            ]);

            $query = Chat::create([
                'uniqid' => $request->uniqid,
                'id_penerima' => 0,
                'nama_penerima' => 'admin',
                'id_pengirim' => Auth::user()->id,
                'nama_pengirim' => Auth::user()->name,
                'message' => $request->message
            ]);
        } else {

            RoomChat::where('uniqid', $getRoom->uniqid)->update([
                'status' => 'New',
            ]);

            $query = Chat::create([
                'uniqid' => $getRoom->uniqid,
                'id_penerima' => 0,
                'nama_penerima' => 'admin',
                'id_pengirim' => Auth::user()->id,
                'nama_pengirim' => Auth::user()->name,
                'message' => $request->message
            ]);
        }

        if ($query) {
            return response()->json([
                '_status' => 200,
                '_data' => $query,
            ]);
            }
        return 'error';
    }

    public function adm_get_room_chat()
    {
        $query = RoomChat::get();
        
        if ($query) {
            return response()->json([
                '_status' => 200,
                '_data' => $query,
            ]);
            }
        return 'error';
    }

    public function adm_get_chat($uniqid)
    {
        $query['chat'] = Chat::where('uniqid', $uniqid)->get();

        if ($query['chat']) {
            if ($query['chat'][0]->id_penerima == 0) {
                $query['user'] = User::where('id', Auth::user()->id)->first();
                $query['guest'] = User::where('id', $query['chat'][0]->id_pengirim)->first();
            } else {
                $query['guest'] = User::where('id', Auth::user()->id)->first();
                $query['user'] = User::where('id', $query['chat'][0]->id_pengirim)->first();
            }
        }
        
        if ($query) {
            return response()->json([
                '_status' => 200,
                '_data' => $query,
            ]);
            }
        return 'error';
    }

    public function adm_kirim_chat(Request $request)
    {
        $query = Chat::create([
            'uniqid' => $request->uniqid,
            'id_penerima' => $request->id_penerima,
            'nama_penerima' => $request->nama_penerima,
            'id_pengirim' => 0,
            'nama_pengirim' => 'admin',
            'message' => $request->message,
        ]);

        if ($query) {
            return response()->json([
                '_status' => 200,
                '_data' => $query,
            ]);
            }
        return 'error';
    }
}
