<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller as Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileControllers extends Controller
{
    public function profile()
    {
        $query = User::where('id', Auth::user()->id)->first();

        if ($query) {
        return response()->json([
            '_status' => 200,
            '_data' => $query,
        ]);
        }
        return 'error';
    }

    public function updateUser(Request $request)
    {
        $picture = $this->uploadFile($request->file);
        $query = User::where('id', Auth::user()->id)->update([
            'name' => $request->name,
            'alamat' => $request->alamat,
            'telepon' => $request->telepon,
            'tempat_tanggal_lahir' => $request->tempat_tanggal_lahir,
            'agama' => $request->agama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'no_ktp' => $request->no_ktp,
            'nik' => $request->nik,
            'email' => $request->email,
            'password' => $request->password,
            'picture' => $picture,
        ]);

        if ($query) {
            return response()->json([
                '_status' => 200,
                '_message' => 'Data diri berhasil di ubah',
            ]);
        }
        return 'error';
    }
}
