<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller as Controller;
use App\Models\Surat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class SuratControllers extends Controller
{

    public function get_surat()
    {
        $query = Surat::where('user_id', Auth::user()->id)->get();
        return response()->json([
            '_status' => 200,
            '_data' => $query,
        ]);
    }

    public function buat_surat(Request $request)
    {

        $checkRows = Surat::where('kode_surat', $request->kode_surat)->first();

        if (empty($checkRows)) {
        
            ($request->fcp_kk != null) ? $fcp_kk = $this->uploadFile($request->fcp_kk) : $fcp_kk = null;
            ($request->fcp_ktp != null) ? $fcp_ktp = $this->uploadFile($request->fcp_ktp) : $fcp_ktp = null;
            ($request->fcp_jps != null) ? $fcp_jps = $this->uploadFile($request->fcp_jps) : $fcp_jps = null;
            ($request->fcp_ktp_ayahibu != null) ? $fcp_ktp_ayahibu = $this->uploadFile($request->fcp_ktp_ayahibu) : $fcp_ktp_ayahibu = null;

            Surat::create([
                'kode_surat' => $request->kode_surat,
                'user_id' => Auth::user()->id,
                'nama_lengkap' => Auth::user()->name,

                'email' => Auth::user()->email,
                'telepon' => Auth::user()->telepon,
                'alamat' => Auth::user()->alamat,
                'agama' => Auth::user()->agama,
                'jenis_kelamin' => Auth::user()->jenis_kelamin,
                'tempat_tanggal_lahir' => Auth::user()->tempat_tanggal_lahir,
                'nik' => Auth::user()->nik,
                'no_ktp' => Auth::user()->no_ktp,
                'picture' => Auth::user()->picture,

                'jenis_surat' => $request->jenis_surat,
                'penghasilan' => $request->penghasilan,
                'pekerjaan' => $request->pekerjaan,

                'pendidikan' => $request->pendidikan,
                'status_perkawinan' => $request->status_perkawinan,
                'kewarganegaraan' => $request->kewarganegaraan,
                'orpol_ormal' => $request->orpol_ormal,

                'fcp_kk' => $fcp_kk,
                'fcp_ktp' => $fcp_ktp,
                'fcp_jps' => $fcp_jps,
                'fcp_ktp_ayahibu' => $fcp_ktp_ayahibu,
                'status' => 'Diproses'
            ]);
            return response()->json([
                '_status' => 200,
                '_message' => 'Permintaan Surat Berhasil Dikirimkan',
                '_data' => $request->kode_surat,
            ]);
        }
        return 'error';
    }

    public function detail_surat($kode)
    {
        $data = Surat::where('kode_surat', $kode)->first();
        if ($data) {
            return response()->json([
                '_status' => 200,
                '_data' => $data,
            ]);
        }
        return 'error';
    }

    public function adm_get_surat($params)
    {
        if ($params == 'semua') {
            $data = Surat::get();
        } else {
            $data = Surat::where('status', $params)->get();
        }
        
        if ($data) {
            return response()->json([
                '_status' => 200,
                '_data' => $data,
            ]);
        }
        return 'error';
    }

    public function proses_surat(Request $request)
    {
        $data = Surat::where('kode_surat', $request->kode_surat)->update([
            'status' => $request->status,

            'tanggal_diambil' => $request->tanggal_diambil,
            'pengambil' => $request->pengambil,
            'alasan_ditolak' => $request->alasan_ditolak,

            'notifikasi' => 'false'
        ]);
        if ($data) {
            return response()->json([
                '_status' => 200,
                '_data' => $data,
            ]);
        }
        return 'error';
    }

    public function get_info()
    {
        $query["total"] = Surat::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->get();
        $query["diterima"] = Surat::where([['user_id', Auth::user()->id],['status', 'Diterima']])->get();
        $query["ditolak"] = Surat::where([['user_id', Auth::user()->id],['status', 'Ditolak']])->get();
        $query["diproses"] = Surat::where([['user_id', Auth::user()->id],['status', 'Diproses']])->get();

        if ($query) {
            return response()->json([
                '_status' => 200,
                '_data' => $query,
            ]);
        }
        return 'error';
    }

    public function get_notifikasi()
    {
        $query = Surat::where([['user_id', Auth::user()->id],['status', '!=' ,'Diproses'],['notifikasi', 'false']])->first();

        if ($query) {
            return response()->json([
                '_status' => 200,
                '_data' => $query,
            ]);
        }
        return response()->json([
            '_status' => 200,
            '_data' => 'tidak ada surat',
        ]);
    }

    public function update_notifikasi($kode_surat)
    {
        $query = Surat::where([['user_id', Auth::user()->id],['kode_surat', $kode_surat]])->update([
            'notifikasi' => 'true'
        ]);

        if ($query) {
            return response()->json([
                '_status' => 200,
                '_data' => $query,
            ]);
        }
        return 'error';
    }

    public function delete_surat($id)
    {
        $query = Surat::where('id', $id)->delete();

        if ($query) {
            return response()->json([
                '_status' => 200,
                '_data' => $query,
            ]);
        }
        return 'error';
    }

    public function update_surat(Request $request)
    {

        $query = Surat::where('kode_surat', $request->kode_surat)->update([
            'alamat' => $request->alamat,
            'penghasilan' => $request->penghasilan,
            'pekerjaan' => $request->pekerjaan,
        ]);

        if ($query) {
            return response()->json([
                '_status' => 200,
                '_data' => $query,
            ]);
        }
        return 'error';
    }
}
