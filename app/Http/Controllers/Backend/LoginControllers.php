<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\AuthControllers as Controller;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

use App\Http\Requests\Auth\LoginRequest;

class LoginControllers extends Controller
{
    public function AuthLogin(Request $request)
		{
		  try {
			$request->validate([
			  'email' => 'required',
			  'password' => 'required'
			]);
            
			$user = User::where('email', $request->email)->first();

			if ($request->email != $user->email) {
				return response()->json([
					'_status' => 422,
					'message' => 'User tidak ditemukan',
				  ]);
			}
			
			if ($request->password != $user->password) {
				return response()->json([
					'_status' => 422,
					'message' => 'Password salah',
				  ]);
			}

			$tokenResult = $user->createToken('authToken')->plainTextToken;
			
			Auth::login($user);

			return response()->json([
			  '_status' => 200,
			  '_token' => $tokenResult,
              'token_type' => 'Bearer',
              'roles' => $user->roles,
              'message' => 'Selamat datang kembali ' . $user->name,
			]);
		  } catch (Exception $error) {
			return response()->json([
			  '_status' => 500,
              'message' => $error->getMessage(),
			  'error' => $error,
			]);
		  }
		}

		public function AuthRegister(Request $request)
		{
			$query = User::create([
				'name' => $request->name,
				'alamat' => $request->alamat,
				'telepon' => $request->telepon,
				'tempat_tanggal_lahir' => $request->tempat_tanggal_lahir,
				'agama' => $request->agama,
				'jenis_kelamin' => $request->jenis_kelamin,
				'no_ktp' => $request->no_ktp,
				'nik' => $request->nik,
				'email' => $request->email,
				'remember_token' => Carbon::now(),
				'email_verified_at' => Carbon::now(),
				'password' => $request->password,
				'roles' => 'user',
				'picture' => 'default.png',
			]);
			if ($query) {
				return response()->json([
					'_status' => 200,
					'_message' => 'Akun berhasil dibuat'
				  ]);
			}
		}

	public function AuthLogout(Request $request)
    {
	Auth::guard('web')->logout();

	$request->session()->invalidate();

	$request->session()->regenerateToken();

	Auth::logout();

	return redirect('/login');
    }
}
