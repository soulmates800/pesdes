<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller as Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UserControllers extends Controller
{
    public function daftar_user()
    {
        $query = User::get();

        return response()->json([
            '_status' => 200,
            '_data' => $query,
        ]);
    }

    public function add_user(Request $request)
    {
        $picture = $this->uploadFile($request->file);
        $query = User::create([
            'name' => $request->name,
            'alamat' => $request->alamat,
            'telepon' => $request->telepon,
            'tempat_tanggal_lahir' => $request->tempat_tanggal_lahir,
            'agama' => $request->agama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'no_ktp' => $request->no_ktp,
            'nik' => $request->nik,
            'email' => $request->email,
            'remember_token' => Carbon::now(),
            'email_verified_at' => Carbon::now(),
            'password' => $request->password,
            'roles' => $request->roles,
            'picture' => $picture,
        ]);

        if ($query) {
            return response()->json([
                '_status' => 200,
                '_data' => $query,
                '_message' => 'User berhasil di tambahkan !'
            ]);
        }
        return 'error';
    }

    public function update_list_user(Request $request)
    {
        $query = User::where('id', $request->id_user)->update([
            'password' => $request->password
        ]);

        if ($query) {
            return response()->json([
                '_status' => 200,
                '_data' => $query,
                '_message' => 'Password berhasil diubah !'
            ]);
        }
        return 'error';
    }

    public function delete_user(Request $request)
    {
        $query = User::where('id', $request->id_user)->delete();

        if ($query) {
            return response()->json([
                '_status' => 200,
                '_data' => $query,
                '_message' => 'Success'
            ]);
        }
        return 'error';
    }
}
