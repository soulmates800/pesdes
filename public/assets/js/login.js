var csrf_token = $('meta[name="csrf-token"]').attr('content');

function loginAuth() {
    var form = $('#authLogin')[0];
    var data = new FormData(form);
    data.append("_token", csrf_token );

    $('.btn_login_costum').prop('disabled', true)
    $('.btn_login_costum').css('opacity', 0.5)
    $('.btn_login_costum').html('Loading ...')

    $.ajax({
        data: data,
        url: ServeUrl + "/login",
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        complete: function(response) {

            $('.btn_login_costum').prop('disabled', false)
            $('.btn_login_costum').css('opacity', 1)
            $('.btn_login_costum').html('Masuk')

            if (response.responseJSON._status == 200) {
                if (response.responseJSON.roles == 'admin') {
                    window.location.href = BaseUrl + '/admin/home';
                } else {
                    window.location.href = BaseUrl + '/user/home';
                }
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal Login !',
                    html: response.responseJSON.message,
                    confirmButtonColor: "#0D6EFD",
                    confirmButtonText: 'Baiklah',
                    onClose: function() {
                        $('#placeHold').val('')
                    }
                  })
            }
        },
        dataType: 'json'
    })
}

function redirectRegister() {
    window.location.href = BaseUrl + '/register'
}
