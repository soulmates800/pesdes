

function getRoomChat() {
    $.ajax({
        url: ServeUrl + '/admin/get_room_chat',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        complete: function(response) {
            if (response.responseJSON._status == 200) {
                var html = '';
                
                var inc = 1;
                for (let i = 0; i < response.responseJSON._data.length; i++) {
                    html += '<tr>';
                    html += '<th scope="row">'+inc+'</th>';
                    html += '<td>'+response.responseJSON._data[i].nama_user+'</td>';
                    html += '<td>'+response.responseJSON._data[i].uniqid+'</td>';

                    if (response.responseJSON._data[i].status == 'New') {
                        html += '<td><span class="badge bg-primary">'+response.responseJSON._data[i].status+'</span></td>';
                    } else {
                        html += '<td><span class="badge bg-danger">'+response.responseJSON._data[i].status+'</span></td>';
                    }
                    
                    html += '<td>';
                    html += '<span type="button" onclick="detailChat(`'+response.responseJSON._data[i].uniqid+'`)" data-toggle="modal" data-target="#bottom_modal" class="badge bg-info">Reply</span>';
                    html += '</td>';
                    html += '</tr>';
                    inc++
                }
                $('#generateData').html(html);
            } else {
                console.log("Something Wrong");
            }
        },error: function(xhr, status, error) {
            console.log("xhr", xhr);
            console.log("status", status);
            console.log("error", error);
        },
    })
}
getRoomChat()

function formatDate(date){
    let tanggal = moment(date, 'YYYY-MM-DD HH:mm:ss').format('DD-MMMM-YYYY');
    return tanggal;
  }

  function detailChat(id) {
      window.location.href = BaseUrl + '/admin/detail_chat/' + id;
  }