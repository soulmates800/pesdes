function getProfile() {
    $.ajax({
        data: '',
        url: ServeUrl + "/admin/profile",
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        complete: function(response) {
            
            var data = response.responseJSON._data;
            if (response.responseJSON._status == 200) {
                
 
                $('#name').val(data.name);
                $('#email').val(data.email);
                $('#telepon').val(data.telepon);
                $('#password').val(data.password);
                $('#alamat').val(data.alamat);
                $('#agama').val(data.agama);
                $('#jenis_kelamin').val(data.jenis_kelamin);
                $('#tempat_tanggal_lahir').val(data.tempat_tanggal_lahir);
                $('#nik').val(data.nik);
                $('#no_ktp').val(data.no_ktp);
                $('#picture-show').attr('src', BaseUrl + '/assets/image/data/' + data.picture);
                
            } else {
                console.log('error')
            }
        },
        dataType: 'json'
    })
}
getProfile()

FilePond.registerPlugin(FilePondPluginImagePreview, FilePondPluginFileValidateType);

pond = FilePond.create(
    document.querySelector('#image-tickets'), {
        allowMultiple: true,
        instantUpload: false,
        allowProcess: false,
        dropValidation: true,
        acceptedFileTypes: ['image/*'],
});

$("#updateUser").submit(function(event) {
    event.preventDefault();
    
    var form = $('#updateUser')[0];
    var data = new FormData(form);

    var pondFiles = pond.getFiles();
    for (var i = 0; i < pondFiles.length; i++) {
        data.append('file', pondFiles[i].file);
    }

    $.ajax({
        data: data,
        url: ServeUrl + "/admin/updateUser",
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        complete: function(response) {
            console.log('response', response)
            if (response.responseJSON._status == 200) {
                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil !',
                    text: response.responseJSON._message,
                    confirmButtonColor: "#0D6EFD",
                    confirmButtonText: 'Baiklah',
                    onClose: function() {
                        getProfile();
                        pond.removeFiles();
                    }
                  })
            } else {
                console.log('error')
            }
        },
        dataType: 'json'
    })
});

function formatDate(date){
    let tanggal = moment(date, 'YYYY-MM-DD HH:mm:ss').format('DD-MMMM-YYYY');
    return tanggal;
}

function validateFileType(input){
    readURL(input)
    var fileName = $('#customFile').val();
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
        $('#file-name').html(fileName);
    }else{
        alert("Hanya gambar berformat .jpg .jpeg .png yang diizinkan");
    }
}

function readURL(input) {
    console.log(`input`, input)
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {

                var html = ''
                html += '<a href="'+ e.target.result +'" data-lightbox="image-1">';
                html += '<img width="150" height="150" src="'+ e.target.result +'"/>';
                html += '</a>';

                $('#imageFile').html(html)
        };
        reader.readAsDataURL(input.files[0]);
    }
}

