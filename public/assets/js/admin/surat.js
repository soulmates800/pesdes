var dataSurat = [];

function getSurat(params) {
    clearButtonGroup()
    if (params == 'semua') {
        $('#btns_semua').css('background', '#0d6efd')
        $('#btns_semua').css('color', '#fff')
    } else if (params == 'Diproses') {
        $('#btns_diproses').css('background', '#0d6efd')
        $('#btns_diproses').css('color', '#fff')
    } else if (params == 'Diterima') {
        $('#btns_diterima').css('background', '#0d6efd')
        $('#btns_diterima').css('color', '#fff')
    } else {
        $('#btns_ditolak').css('background', '#0d6efd')
        $('#btns_ditolak').css('color', '#fff')
    }

    $.ajax({
        data: '',
        url: ServeUrl + "/admin/get_surat/" + params,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        complete: function(response) {
            console.log('response', response)
            if (response.responseJSON._status == 200) {
                var html = '';
                
                var inc = 1;
                for (let i = 0; i < response.responseJSON._data.length; i++) {
                    dataSurat[i] = response.responseJSON._data[i];
                    html += '<tr>';
                    html += '<th class="text-center align-middle" scope="row">'+inc+'</th>';
                    html += '<td style="text-align: left" class=" align-middle">'+response.responseJSON._data[i].jenis_surat+'</td>';
                    html += '<td style="text-align: left" class=" align-middle">'+response.responseJSON._data[i].nama_lengkap+'</td>';
                    html += '<td class="text-center align-middle">'+formatDate(response.responseJSON._data[i].updated_at)+'</td>';

                    if (response.responseJSON._data[i].status == 'Diproses') {
                        html += '<td class="text-center align-middle"><span class="badge bg-primary">'+response.responseJSON._data[i].status+'</span></td>';
                    } else if (response.responseJSON._data[i].status == 'Diterima') {
                        html += '<td class="text-center align-middle"><span class="badge bg-success">'+response.responseJSON._data[i].status+'</span></td>';
                    } else {
                        html += '<td class="text-center align-middle"><span class="badge bg-danger">'+response.responseJSON._data[i].status+'</span></td>';
                    }

                    
                    html += '<td class="text-center align-middle">';
                    html += '<button type="button" onclick="prosesSurat(`'+i+'`)" data-toggle="modal" data-target="#bottom_modal" class="btn-primary text-white mr-2 shadow" style="border-radius: 5px; border: none;">Proses Surat</button>';
                    html += '</td>';
                    html += '<td class="text-center align-middle">';
                    html += '<button type="button" onclick="deleteSurat(`'+response.responseJSON._data[i].id+'`)" class="btn-danger pl-2 shadow" style="border-radius: 5px; border: none;">Hapus</button>';
                    html += '</td>';
                    html += '</tr>';
                    inc++
                }

                
                $('#generateData').html(html);
            } else {
                console.log('error')
            }
        },
        dataType: 'json'
    })
}
getSurat('semua')

function formatDate(date){
    let tanggal = moment(date, 'YYYY-MM-DD HH:mm:ss').format('DD-MMMM-YYYY');
    return tanggal;
  }

  var currentIndex = 0;
  function prosesSurat(params) {
    currentIndex = params;

    $('.surat-diterima').hide();
    $('.surat-ditolak').hide();
      $('#mdls_kodeSurat').val(dataSurat[params].kode_surat)
        if (dataSurat[params].status == 'Diproses') {
            $('#mdl_status').html('<span class="badge bg-primary">'+dataSurat[params].status+'</span>')
            $('.modal-header').css('background', '#4764E6')
            $("#prosesSubmit").val("Diproses");
            $('.surat-ditolak').hide();
            $('.surat-diterima').hide();
        } else if (dataSurat[params].status == 'Diterima') {
            $('#mdl_status').html('<span class="badge bg-success">'+dataSurat[params].status+'</span>')
            $('.modal-header').css('background', '#47e684')
            $("#prosesSubmit").val("Diterima");
            $('.surat-diterima').show();
            $('#mdl_alamat').val(dataSurat[params].alamat)
            $('#mdl_email').val(dataSurat[params].email)
        } else {
            $('#mdl_status').html('<span class="badge bg-danger">'+dataSurat[params].status+'</span>')
            $('.modal-header').css('background', '#e64747')
            $("#prosesSubmit").val("Ditolak");
            $('.surat-ditolak').show();
        }

        $('#mdl_namaLengkap').val(dataSurat[params].nama_lengkap)

        $('#mdl_alamat').val(dataSurat[params].alamat)
        $('#mdl_email').val(dataSurat[params].email)
        $('#mdl_telepon').val(dataSurat[params].telepon)
        $('#mdl_agama').val(dataSurat[params].agama)
        $('#mdl_jenis_kelamin').val(dataSurat[params].jenis_kelamin)
        $('#mdl_tempat_tanggal_lahir').val(dataSurat[params].tempat_tanggal_lahir)
        $('#mdl_nik').val(dataSurat[params].nik)
        $('#mdl_no_ktp').val(dataSurat[params].no_ktp)
        $('#mdl_foto_profile').attr('src', BaseUrl + '/assets/image/data/' + dataSurat[params].picture)
        $('#mdl_link_foto_profile').attr('href', BaseUrl + '/assets/image/data/' + dataSurat[params].picture)

        $('#mdl_jenisSurat').val(dataSurat[params].jenis_surat)
        
        $('.hidden-input-surat').hide();

        if (dataSurat[params].pendidikan != null) {
            $('.hidden-pendidikan').show();
            $('#mdl_pendidikan').val(dataSurat[params].pendidikan)
        }
      
        if (dataSurat[params].pekerjaan != null) {
            $('.hidden-pekerjaan').show();
            $('#mdl_pekerjaan').val(dataSurat[params].pekerjaan)
        }

        if (dataSurat[params].penghasilan != null) {
            $('.hidden-penghasilan').show();
            $('#mdl_penghasilan').val(dataSurat[params].penghasilan)
        }

        if (dataSurat[params].status_perkawinan != null) {
            $('.hidden-status_perkawinan').show();
            $('#mdl_status_perkawinan').val(dataSurat[params].status_perkawinan)
        }

        if (dataSurat[params].kewarganegaraan != null) {
            $('.hidden-kewarganegaraan').show();
            $('#mdl_kewarganegaraan').val(dataSurat[params].kewarganegaraan)
        }

        if (dataSurat[params].orpol_ormal != null) {
            $('.hidden-orpol_ormal').show();
            $('#mdl_orpol_ormal').val(dataSurat[params].orpol_ormal)
        }

      var html = '';
        if (dataSurat[params].fcp_jps != null) {
            html += '<div class="col-md-2 col-12 text-center">';
            html += '<div class="center_costum">';
            html += '<a href="/assets/image/data/'+dataSurat[params].fcp_jps+'" data-lightbox="image-1" data-title="Jaring Pengaman Sosial (JPS)">';
            html += '<img width="150" height="150" class="imageFcp" class="" src="/assets/image/data/'+dataSurat[params].fcp_jps+'"/>';
            html += '</a>';
            html += '</div>';
            html += '<label class="form-label mt-2">Jaring Pengaman Sosial (JPS)</label>';
            html += '</div>';
        }

        if (dataSurat[params].fcp_kk != null) {
            html += '<div class="col-2 col-12 text-center">';
            html += '<div class="center_costum">';
            html += '<a href="/assets/image/data/'+dataSurat[params].fcp_kk+'" data-lightbox="image-1" data-title="Kartu Keluarga (KK)">';
            html += '<img width="150" height="150" class="imageFcp" src="/assets/image/data/'+dataSurat[params].fcp_kk+'"/>';
            html += '</a>';
            html += '</div>';
            html += '<label class="form-label mt-2">Kartu Keluarga (KK)</label>';
            html += '</div>';
        }

        if (dataSurat[params].fcp_ktp != null) {
            html += '<div class="col-2 col-12 text-center">';
            html += '<div class="center_costum">';
            html += '<a href="/assets/image/data/'+dataSurat[params].fcp_ktp+'" data-lightbox="image-1" data-title="Kartu Tanda Penduduk (KTP)">';
            html += '<img width="150" height="150" class="imageFcp" src="/assets/image/data/'+dataSurat[params].fcp_ktp+'"/>';
            html += '</a>';
            html += '</div>';
            html += '<label class="form-label mt-2">Kartu Tanda Penduduk (KTP)</label>';
            html += '</div>';
        }

      if (dataSurat[params].fcp_ktp_ayahibu != null) {
            html += '<div class="col-2 col-12 pb-3 text-center">';
            html += '<div class="center_costum">';
            html += '<a href="/assets/image/data/'+dataSurat[params].fcp_ktp_ayahibu+'" data-lightbox="image-1" data-title="Kartu Tanda Penduduk (KTP) Ayah dan Ibu">';
            html += '<img width="150" height="150" class="imageFcp" src="/assets/image/data/'+dataSurat[params].fcp_ktp_ayahibu+'"/>';
            html += '</a>';
            html += '</div>';
            html += '<label class="form-label">Kartu Tanda Penduduk (KTP) Ayah dan Ibu</label>';
            html += '</div>';
      }

      $('#generateDatas').html(html);
      handleSelect()
      lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true,
        'alwaysShowNavOnTouchDevices': true,
        'fadeDuration':300,
        'imageFadeDuration': 300
      })
  }

  function handleSelect() {
      var params = $('#prosesSubmit').val();
      switch (params) {
          case 'Diproses':
              $('#prosesSubmit').css('background', '#4764E6')
              $('.surat-ditolak').hide();
              $('.surat-diterima').hide();
              break;
              case 'Diterima':
              $('#prosesSubmit').css('background', '#47e684')
              $('.surat-diterima').show();
              $('.surat-ditolak').hide();
    
              break;
              case 'Ditolak':
              $('#prosesSubmit').css('background', '#e64747')
              $('.surat-ditolak').show();
              $('.surat-diterima').hide();
              break;
      
          default:
              break;
      }
  }

  function submitSurat() {
    
    Swal.fire({
        title: 'Apa kamu yakin?',
        text: "Kamu tidak akan bisa mengembalikannya lagi!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#4764E6',
        cancelButtonColor: '#E64747',
        confirmButtonText: 'Ya, Simpan!',
        cancelButtonText: 'Tidak!'
      }).then((result) => {
        if (result.isConfirmed) {

            var csrf_token = $('meta[name="csrf-token"]').attr('content');
            var formData = new FormData();
            formData.append("kode_surat", dataSurat[currentIndex].kode_surat);
            formData.append("status", $('#prosesSubmit').val());

            formData.append("tanggal_diambil", $('#mdl_tanggal_diambil').val());
            formData.append("pengambil", $('#mdl_pengambil').val());
            formData.append("alasan_ditolak", $('#mdl_alasan_ditolak').val());

            formData.append("_token", csrf_token );
            $.ajax({
                data: formData,
                url: ServeUrl + '/admin/proses_surat',
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                type: 'POST',
                complete: function(response) {
                    console.log('response', response)
                    if (response.responseJSON._status == 200) {
                        getSurat('semua');
                        $('#bottom_modal').modal('hide');
                        $('#closeBtmModal').click();
                    } else {
                        console.log('error')
                    }
                    },
                    dataType: 'json'
                })
        }
      })
	}
  
    function clearButtonGroup() {
        $('#btns_semua').css('background', 'transparent')
        $('#btns_diproses').css('background', 'transparent')
        $('#btns_diterima').css('background', 'transparent')
        $('#btns_ditolak').css('background', 'transparent')
        $('#btns_semua').css('color', '#0d6efd')
        $('#btns_diproses').css('color', '#0d6efd')
        $('#btns_diterima').css('color', '#0d6efd')
        $('#btns_ditolak').css('color', '#0d6efd')
    }

    $(document).ready( function () {
        $('#myTable').DataTable();
    } );

    function deleteSurat(id) {
        Swal.fire({
            title: '<div style="color: white;">Wait !</div>',
            html: '<p class="card-text"><small class="text-muted">Apa kamu benar benar sudah yakin ?<br> Perintah ini akan menghapus untuk <b>selamanya</b></small></p>',
            type: 'warning',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Lakukan!',
            cancelButtonText: 'Tidak, Batalkan!',
            confirmButtonClass: 'btn btn-primary',
            background: '#20232A',
            cancelButtonClass: 'btn btn-danger ml-2',
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: ServeUrl + '/admin/delete_surat/' + id,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    type: 'GET',
                    dataType: 'json',
                    complete: function(response) {
                        if (response.responseJSON._status == 200) {
                            Swal.fire({
                                title: '<div style="color: white;">Berhasil!</div>',
                                icon: 'success',
                                html: '<p class="card-text"><small class="text-muted">Perintah ini berhasil diproses. <br> Lakukan pengecekan ulang pada aksi ini.</small></p>',
                                type: 'success',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: '<i class="fas fa-life-ring fa-sm rotating mr-5"></i>Ya, Baiklah!<i class="fas fa-sm fa-life-ring rotating ml-5"></i>',
                                confirmButtonClass: 'btn btn-second w-btn-100',
                                background: '#14274E',
                                cancelButtonClass: 'btn btn-danger ml-1',
                                onClose: function() {
                                    getSurat()
                                }
                            })
                        } else {
                            console.log("Something Wrong");
                        }
                    },error: function(xhr, status, error) {
                        console.log("xhr", xhr);
                        console.log("status", status);
                        console.log("error", error);
                    },
                })
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire({
                    title: 'Oops !',
                    icon: 'error',
                    html: '<p class="card-text"><small class="text-muted">Data ini tetap akan kami simpan</small></p>',
                    type: 'error',
                    confirmButtonText: 'Thanks !',
                    confirmButtonClass: 'btn btn-success',
                    })
                }
            })
    }