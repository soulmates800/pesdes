var detailDataUser = [];
function getUser() {
    $.ajax({
        data: '',
        url: ServeUrl + "/admin/daftar_user/",
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        complete: function(response) {
            if (response.responseJSON._status == 200) {
                
                var data = response.responseJSON._data;

                var html = '';
                var inc = 1;

                var countAdmin = 0;
                var countUser = 0;
                for (let i = 0; i < data.length; i++) {
                    detailDataUser[i] = data[i]

                    html += '<a onClick="detailUser('+i+')" style="cursor: pointer"><div class="single-review-st-text mt-3 mb-3 border-bottom">';
                    html += '<div class="row"><div class="col-md-2 col-2"><img style="width: 50px; height: 50px;" src="'+ BaseUrl + '/assets/image/data/' + response.responseJSON._data[i].picture +'" alt=""></div>';
                    html += '<div class="col-md-7 col-10">';
                    html += '<h6 class="userHover">'+data[i].name+'</h6>';
                    html += '<p>'+data[i].email+'</p>';

                    if (data[i].roles == 'admin') {
                        html += '</div><div class="col-md-3 mb-3" style="text-align: right;"><span class="badge bg-danger" ">'+data[i].roles+'</span></div>';
                        countAdmin++;
                    } else {
                        html += '</div><div class="col-md-3 mb-3" style="text-align: right;"><span class="badge bg-primary" ">'+data[i].roles+'</span></div>';
                        countUser++;
                    }

                    if (i == 0) {
                        detailUser(i)
                    }
                    
                    html += '</div></div></div></a>';
                    inc++;

                    
                }
                $('#generateUser').html(html);
            } else {
                console.log('error')
            }
        },
        dataType: 'json'
    })
}
getUser()

var currentId = 0;
function detailUser(id) {
    currentId = id;

    $('#namaUser').html(detailDataUser[id].name)
    $('#emailUser').html(detailDataUser[id].email)
    $('#namaUser2').html(detailDataUser[id].name)
    $('#teleponUser').html(detailDataUser[id].telepon)
    $('#passwordUser').html(detailDataUser[id].password)
    $('#alamatUser').html(detailDataUser[id].alamat)
    $('#agamaUser').html(detailDataUser[id].agama)
    $('#jeniskelaminUser').html(detailDataUser[id].jenis_kelamin)
    $('#tanggalahirUser').html(detailDataUser[id].tempat_tanggal_lahir)
    $('#nikUser').html(detailDataUser[id].nik)
    $('#ktpUser').html(detailDataUser[id].no_ktp)


    $('#p_roles').html(detailDataUser[id].roles)
    $('#pictureUser').attr('src', BaseUrl + '/assets/image/data/' + detailDataUser[id].picture)

    
    $('#placeHold').attr('placeholder', 'Password baru untuk ' + detailDataUser[id].name)

    $('#deleteUserButton').html('Hapus User ' + detailDataUser[id].name)
}
var csrf_token = $('meta[name="csrf-token"]').attr('content');
function changePassword() {
    var request = new FormData();
    request.append("id_user", detailDataUser[currentId].id );
    request.append("password", $('#placeHold').val() );
    request.append("_token", csrf_token );
    
    $.ajax({
        data: request,
        url: ServeUrl + '/admin/update_list_user',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        dataType: 'json',
        complete: function(response) {
            if (response.responseJSON._status == 200) {

                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil !',
                    html: '<p class="card-text"><small class="text-muted"><b>Hmm...</b> sepertinya kamu melakukan sesuatu.<br> Perintah yang baru saja kamu lakukan <b>berhasil</b> di proses.</small></p>',
                    confirmButtonColor: "#0D6EFD",
                    confirmButtonText: 'Baiklah',
                    onClose: function() {
                        // $('#placeHold').val('')
                        window.location.reload()
                    }
                  })

            } else {
                console.log("Something Wrong");
            }
        },error: function(xhr, status, error) {
            console.log("xhr", xhr);
            console.log("status", status);
            console.log("error", error);
        },
    })
}

function formatDate(date) {
    let tanggal = moment(date, 'YYYY-MM-DD HH:mm:ss').format('dddd,DD MMMM');
    return tanggal;
  }

  function deleteUser() {
      var request = new FormData();
      request.append("id_user", detailDataUser[currentId].id );
      request.append("_token", csrf_token );
      
      $.ajax({
          data: request,
          url: ServeUrl + '/admin/delete_user',
          processData: false,
          contentType: false,
          cache: false,
          timeout: 600000,
          type: 'POST',
          dataType: 'json',
          complete: function(response) {
              if (response.responseJSON._status == 200) {

                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil !',
                    html: '<p class="card-text"><small class="text-muted"><b>Hmm...</b> sepertinya kamu melakukan sesuatu.<br> Perintah yang baru saja kamu lakukan <b>berhasil</b> di proses.</small></p>',
                    confirmButtonColor: "#0D6EFD",
                    confirmButtonText: 'Baiklah',
                    onClose: function() {
                        $('#placeHold').val('')
                        getUser()
                    }
                  })

              } else {
                  console.log("Something Wrong");
              }
          },error: function(xhr, status, error) {
              console.log("xhr", xhr);
              console.log("status", status);
              console.log("error", error);
          },
      })
  }