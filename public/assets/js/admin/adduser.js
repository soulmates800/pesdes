FilePond.registerPlugin(FilePondPluginImagePreview, FilePondPluginFileValidateType);

pond = FilePond.create(
    document.querySelector('#image-tickets'), {
        allowMultiple: true,
        instantUpload: false,
        allowProcess: false,
        dropValidation: true,
        acceptedFileTypes: ['image/*'],
});

$("#addUser").submit(function(event) {
    event.preventDefault();
    
    var form = $('#addUser')[0];
    var data = new FormData(form);

    var pondFiles = pond.getFiles();
    for (var i = 0; i < pondFiles.length; i++) {
        data.append('file', pondFiles[i].file);
    }

    $.ajax({
        data: data,
        url: ServeUrl + "/admin/add_user",
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        complete: function(response) {
            console.log('response', response)
            if (response.responseJSON._status == 200) {
                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil !',
                    text: response.responseJSON._message,
                    confirmButtonColor: "#0D6EFD",
                    confirmButtonText: 'Baiklah',
                    onClose: function() {
                        window.location.reload()
                    }
                  })
            } else {
                console.log('error')
            }
        },
        dataType: 'json'
    })
});