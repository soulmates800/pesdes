

var data = []
function getMessage() {
    
    $.ajax({
        url: ServeUrl + '/admin/adm_get_chat/' + window.location.pathname.split('/').pop(),
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        complete: function(response) {
            if (response.responseJSON._status == 200) {
    
                data = response.responseJSON._data;
                var html = '';
                for (let i = 0; i < data.chat.length; i++) {

                    if (data.chat[i].id_pengirim == 0) {

                        html += '<div class="col-md-12 col-12 pb-4">';
                        html += '<div class="container-main-chat-user">';
                        html += '<div class="container-chat-user">';
                        html += '<span style="margin-right: 10px; font-weight: bolder">'+data.user.name+'</span><img class="image-chat-admin " src="'+ BaseUrl + '/assets/image/data/'+ data.user.picture +'" alt="" />';
                        html += '</div>';
                        html += '<div class="container-body-chat-user">';
                        html += data.chat[i].message;
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';

                    } else {
 
                        html += '<div class="col-md-12 col-12 pb-4">';
                        html += '<div class="container-main-chat-guest">';
                        html += '<div class="container-chat-admin">';
                        html += '<img class="image-chat-admin" src="'+ BaseUrl + '/assets/image/data/'+ data.guest.picture +'" alt="" />'+ data.guest.name;
                        html += '</div>';
                        html += '<div class="container-body-chat-admin">';
                        html += data.chat[i].message;
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                    }
                    
                    $('#generateChat').html(html)
                    $('#generateChat').scrollTop($('#generateChat')[0].scrollHeight);
                    
                }

            } else {
                console.log("Something Wrong");
            }
        },error: function(xhr, status, error) {
            console.log("xhr", xhr);
            console.log("status", status);
            console.log("error", error);
        },
    })
}
getMessage()

function kirimPesanChat() {
    $('#buttonKirim').attr('disabled', true)
    $('#buttonKirim').html('Mengirim ...')
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var request = new FormData();
    request.append("message", $('#message').val() );
    request.append("uniqid", window.location.pathname.split('/').pop() );
    request.append("id_penerima", data.guest.id );
    request.append("nama_penerima", data.guest.name );
    request.append("_token", csrf_token );
    
    $.ajax({
        data: request,
        url: ServeUrl + '/admin/adm_kirim_chat',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        dataType: 'json',
        complete: function(response) {
            $('#buttonKirim').attr('disabled', false)
            $('#buttonKirim').html('Kirim ...')
            if (response.responseJSON._status == 200) {
                getMessage()
                $('#message').val('')
            } else {
                console.log("Something Wrong");
            }
        },error: function(xhr, status, error) {
            console.log("xhr", xhr);
            console.log("status", status);
            console.log("error", error);
        },
    })
}

var input = document.getElementById("message");

// Execute a function when the user releases a key on the keyboard
input.addEventListener("keyup", function(event) {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    document.getElementById("buttonKirim").click();
  }
});