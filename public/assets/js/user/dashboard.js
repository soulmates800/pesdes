function getInfo() {
    $.ajax({
        url: ServeUrl + '/user/get_info',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        complete: function(response) {
            if (response.responseJSON._status == 200) {
                console.log('object', response.responseJSON._data)

                $('#totalSurat').html(response.responseJSON._data.total.length)
                $('#diterima').html(response.responseJSON._data.diterima.length)
                $('#ditolak').html(response.responseJSON._data.ditolak.length)
                $('#diproses').html(response.responseJSON._data.diproses.length)

                var html = '';

                if (response.responseJSON._data.total.length == 0) {
                    html += '<div class="container_null_data">';
                    html += '<span>Tidak ada data, kamu belum mengajukan surat apapun</span>';
                    html += '</div>';
                    
                } else {
                for (let i = 0; i < response.responseJSON._data.total.length; i++) {

                    
                    


                        html += '<div class="d-flex text-muted pt-3">';
                    html += '<svg class="bd-placeholder-img flex-shrink-0 me-2 rounded" width="32" height="32" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: 32x32" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#4764E6"/><text x="50%" y="50%" fill="#4764E6" dy=".3em">32x32</text></svg>';
                    html += '<p class="pb-3 mb-0 small lh-sm border-bottom">';
                    

                    switch (response.responseJSON._data.total[i].status) {
                        case 'Diproses':
                            html += '<strong class="d-block text-gray-dark">Diproses</strong>';
                            html += 'Anda membuat surat '+response.responseJSON._data.total[i].jenis_surat+' pada '+formatDate(response.responseJSON._data.total[i].created_at)+' dan sedang dalam proses';
                            break;
                        case 'Ditolak':
                            html += '<strong class="d-block text-gray-dark">Ditolak</strong>';
                            html += 'Mohon maaf kami menolak surat '+response.responseJSON._data.total[i].jenis_surat+' anda pada '+formatDate(response.responseJSON._data.total[i].created_at);
                            break;
                        case 'Diterima':
                            html += '<strong class="d-block text-gray-dark">Diterima</strong>';
                            html += 'Surat '+response.responseJSON._data.total[i].jenis_surat+' telah kami terima pada '+formatDate(response.responseJSON._data.total[i].updated_at);
                            break;
                        default:
                            console.log('error');
                            break;
                    }

                    html += '</p>';
                    html += '</div>';

                    }

                    
                }

                $('#generateLogData').html(html)

            } else {
                console.log("Something Wrong");
            }
        },error: function(xhr, status, error) {
            console.log("xhr", xhr);
            console.log("status", status);
            console.log("error", error);
        },
    })
}
getInfo()


  function formatDate(date) {
    let tanggal = moment(date, 'YYYY-MM-DD HH:mm:ss').format('dddd,DD MMMM');
    return tanggal;
  }