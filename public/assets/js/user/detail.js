$('.diterima-surat').hide();
$('.ditolak-surat').hide();

function getDetail() {
    $.ajax({
        data: '',
        url: ServeUrl + "/user/detail_surat/" + window.location.pathname.split('/').pop(),
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        complete: function(response) {
            var data = response.responseJSON._data
            if (response.responseJSON._status == 200) {
                $('#dt_kodeSurat').html(data.kode_surat)
                $('#dt_nama_lengkap').html(data.nama_lengkap)

                $('#dt_email').html(data.email)
                $('#dt_telepon').html(data.telepon)
                $('#dt_agama').html(data.agama)
                $('#dt_jenis_kelamin').html(data.jenis_kelamin)
                $('#dt_tempat_tanggal_lahir').html(data.tempat_tanggal_lahir)
                $('#dt_nik').html(data.nik)
                $('#dt_no_ktp').html(data.no_ktp)

                $('#dt_jenis_surat').html(data.jenis_surat)
                $('#dt_alamat').html(data.alamat)
                $('#dt_kodeSurat').html(data.kode_surat)
                $('#foto_profile').attr('src', BaseUrl + '/assets/image/data/' + data.picture)
                $('#foto-profile-light').attr('href', BaseUrl + '/assets/image/data/' + data.picture)

                if (data.status == 'Diterima') {
                    $('.diterima-surat').show();
                    $('#dt_status').html('<span class="badge bg-success">'+data.status+'</span>')
                    $('#dt_tanggal_diambil').html(data.tanggal_diambil)
                    $('#dt_pengambil').html(data.pengambil)
                } else if (data.status == 'Ditolak') {
                    $('.ditolak-surat').show();
                    $('#dt_alasan_ditolak').html(data.alasan_ditolak)
                    $('#dt_status').html('<span class="badge bg-danger">'+data.status+'</span>')
                } else {
                    $('.diterima-surat').hide();
                    $('.ditolak-surat').hide();
                    $('#dt_status').html('<span class="badge bg-primary">'+data.status+'</span>')
                }

                var html = '';

                if (data.penghasilan != null) {
                    html += '<div class="col-3 pb-3 border_bottom">';
                    html += '<span class="title_label">Penghasilan</span>';
                    html += '</div>';
                    html += '<div class="col-9 pb-3 border_bottom">';
                    html += '<div id="dt_penghasilan">'+data.penghasilan+'</div>';
                    html += '</div>';
                }

                if (data.pekerjaan != null) {
                    html += '<div class="col-3 pb-3 border_bottom">';
                    html += '<span class="title_label">Pekerjaan</span>';
                    html += '</div>';
                    html += '<div class="col-9 pb-3 border_bottom">';
                    html += '<div id="dt_pekerjaan">'+data.pekerjaan+'</div>';
                    html += '</div>';
                }

                if (data.fcp_jps != null) {
                    html += '<div class="col-3 pb-3 border_bottom">';
                    html += '<span class="title_label">Jaring Pengaman Sosial (JPS)</span>';
                    html += '</div>';
                    html += '<div class="col-9 pb-3 border_bottom">';
                    html += '<a href="'+ BaseUrl + '/assets/image/data/'+data.fcp_jps+'" data-lightbox="image-1" data-title="Jaring Pengaman Sosial (JPS)">';
                    html += '<img class="imageFcp" src="/assets/image/data/'+data.fcp_jps+'"/>';
                    html += '</a>';
                    html += '</div>';
                }

                if (data.fcp_kk != null) {
                    html += '<div class="col-3 pb-3 border_bottom">';
                    html += '<span class="title_label">Kartu Keluarga (KK)</span>';
                    html += '</div>';
                    html += '<div class="col-9 pb-3 border_bottom">';
                    html += '<a href="'+ BaseUrl + '/assets/image/data/'+data.fcp_kk+'" data-lightbox="image-1" data-title="Kartu Keluarga (KK)">';
                    html += '<img class="imageFcp" src="'+ BaseUrl + '/assets/image/data/'+data.fcp_kk+'"/>';
                    html += '</a>';
                    html += '</div>';
                }

                if (data.fcp_ktp != null) {
                    html += '<div class="col-3 pb-3 border_bottom">';
                    html += '<span class="title_label">Kartu Tanda Penduduk (KTP)</span>';
                    html += '</div>';
                    html += '<div class="col-9 pb-3 border_bottom">';
                    html += '<a href="'+ BaseUrl + '/assets/image/data/'+data.fcp_ktp+'" data-lightbox="image-1" data-title="Kartu Tanda Penduduk (KTP)">';
                    html += '<img class="imageFcp" src="'+ BaseUrl + '/assets/image/data/'+data.fcp_ktp+'"/>';
                    html += '</a>';
                    html += '</div>';
                }

                if (data.fcp_ktp_ayahibu != null) {
                    html += '<div class="col-3 pb-3 border_bottom">';
                    html += '<span class="title_label">Kartu Tanda Penduduk (KTP) Ayah dan Ibu</span>';
                    html += '</div>';
                    html += '<div class="col-9 pb-3 border_bottom">';
                    html += '<a href="'+ BaseUrl + '/assets/image/data/'+data.fcp_ktp_ayahibu+'" data-lightbox="image-1" data-title="Kartu Tanda Penduduk (KTP) Ayah dan Ibu">';
                    html += '<img class="imageFcp" src="'+ BaseUrl + '/assets/image/data/'+data.fcp_ktp_ayahibu+'"/>';
                    html += '</a>';
                    html += '</div>';
                }

                $('#generateData').html(html);

                

                lightbox.option({
                    'resizeDuration': 200,
                    'wrapAround': true,
                    'alwaysShowNavOnTouchDevices': true,
                    'fadeDuration':300,
                    'imageFadeDuration': 300
                  })
            } else {
                console.log('error')
            }
        },
        dataType: 'json'
    })
}
getDetail();

function back() {
    window.location.href = BaseUrl + '/user/surat/';
}