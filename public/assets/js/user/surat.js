var selected_option = $('#jenis_surat option:selected');

var jumlahDomisili = 0;
var jumlahSkck = 0;
var jumlahSktm = 0;
$('.check-surat').hide()
var selectJenisSurat = '';
function handleJenisSurat() {
    
    var selectedValue = $('#jenis_surat').val()
    $('.check-surat').show()
    switch (selectedValue) {
        case "Surat Domisili":
            buatKodeSurat('DSL', jumlahDomisili)
            var html = '';

            html += '<div class="col-md-6 pb-3 border_bottom">';
            html += '<label class="form-label">Pekerjaan</label>';
            html += '<input type="text" class="form-control" required name="pekerjaan">';
            html += '</div>';

            html += '<div class="col-md-6 pb-3 border_bottom">';
            html += '<label class="form-label">Status Perkawinan</label>';
            html += '<input type="text" class="form-control" required name="status_perkawinan">';
            html += '</div>';

            html += '<div class="col-md-6 pb-3 border_bottom">';
            html += '<label class="form-label">Fotocopy Kartu Keluarga (KK)</label>';
            html += '<input class="form-control" type="file" name="fcp_kk" required id="fcp_kkDSL" onchange="validateFileType(this)" accept="image/png, image/jpeg, image/jpg" />';
            html += '</div>';

            html += '<div class="col-md-6 pb-3 border_bottom">';
            html += '<label class="form-label">Fotocopy Kartu Tanda Penduduk (KTP)</label>';
            html += '<input class="form-control" type="file" name="fcp_ktp" required id="fcp_ktpDSL" onchange="validateFileType(this)" accept="image/png, image/jpeg, image/jpg">';
            html += '</div>';
            $('#generateJenisSurat').html(html);
        break;

        case "Surat Keterangan Tidak Mampu":
            buatKodeSurat('SKTM', jumlahSktm)
            var html = '';
            html += '<div class="col-md-6 pb-3 border_bottom">';
            html += '<label class="form-label">Penghasilan</label>';
            html += '<input type="text" class="form-control" required name="penghasilan">';
            html += '</div>';
            html += '<div class="col-md-6 pb-3 border_bottom">';
            html += '<label class="form-label">Pekerjaan</label>';
            html += '<input type="text" class="form-control" required name="pekerjaan">';
            html += '</div>';
            html += '<div class="col-md-6 pb-3 border_bottom">';
            html += '<label class="form-label">Fotocopy Kartu Keluarga (KK)</label>';
            html += '<input class="form-control" type="file" name="fcp_kk" required id="fcp_kkSKTM" onchange="validateFileType(this)" accept="image/png, image/jpeg, image/jpg">';
            html += '</div>';
            html += '<div class="col-md-6 pb-3 border_bottom">';
            html += '<label class="form-label">Fotocopy Kartu Tanda Penduduk (KTP)</label>';
            html += '<input class="form-control" type="file" name="fcp_ktp" required id="fcp_ktpSKTM" onchange="validateFileType(this)" accept="image/png, image/jpeg, image/jpg">';
            html += '</div>';
            html += '<div class="col-md-12 pb-3 border_bottom">';
            html += '<label class="form-label">Fotocopy Jaring Pengaman Sosial (JPS)</label>';
            html += '<input class="form-control" type="file" name="fcp_jps" required id="fcp_jpsSKTM" onchange="validateFileType(this)" accept="image/png, image/jpeg, image/jpg">';
            html += '</div>';
            
            $('#generateJenisSurat').html(html);
        break;

        case "Surat Keterangan Catatan Kepolisian":
            buatKodeSurat('SKCK', jumlahSkck)
            var html = '';

            html += '<div class="col-md-6 pb-3 border_bottom">';
            html += '<label class="form-label">Pendidikan</label>';
            html += '<input type="text" class="form-control" required name="pendidikan">';
            html += '</div>';

            html += '<div class="col-md-6 pb-3 border_bottom">';
            html += '<label class="form-label">Pekerjaan</label>';
            html += '<input type="text" class="form-control" required name="pekerjaan">';
            html += '</div>';
            
            html += '<div class="col-md-6 pb-3 border_bottom">';
            html += '<label class="form-label">Kewarganegaraan</label>';
            html += '<input type="text" class="form-control" required name="kewarganegaraan">';
            html += '</div>';

            html += '<div class="col-md-6 pb-3 border_bottom">';
            html += '<label class="form-label">Orpol / Ormal</label>';
            html += '<input type="text" class="form-control" required name="orpol_ormal">';
            html += '</div>';

            html += '<div class="col-md-6 pb-3 border_bottom">';
            html += '<label class="form-label">Fotocopy Kartu Keluarga (KK)</label>';
            html += '<input class="form-control" type="file" name="fcp_kk" required id="fcp_kkSKCK" onchange="validateFileType(this)" accept="image/png, image/jpeg, image/jpg">';
            html += '</div>';
            html += '<div class="col-md-6 pb-3 border_bottom">';
            html += '<label class="form-label">Fotocopy Kartu Tanda Penduduk (KTP) Ayah dan Ibu</label>';
            html += '<input class="form-control" type="file" name="fcp_ktp_ayahibu" required id="fcp_ktp_ayahibuSKCK" onchange="validateFileType(this)" accept="image/png, image/jpeg, image/jpg">';
            html += '</div>';
            $('#generateJenisSurat').html(html);
        break;
    
        default:
            var html = '';
            $('#generateJenisSurat').html(html);
        break;
    }
}

function makeRandomText(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }

 var kodeSurat = '';
 function buatKodeSurat(selectJenisSurat, jumlahSurat) {
    var tanggalKode = moment().format('DDMMYY');
    kodeSurat = selectJenisSurat + '-' + tanggalKode + '-' + jumlahSurat

    $('#kodeSurats').html('Kode Surat: ' + kodeSurat);
 }

 $("#buatSurat").submit(function(event) {
    event.preventDefault();
    
    var form = $('#buatSurat')[0];
    var data = new FormData(form);
    data.append("kode_surat", kodeSurat );
    $.ajax({
        data: data,
        url: ServeUrl + "/user/buat_surat",
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        complete: function(response) {
            console.log('response', response)
            if (response.responseJSON._status == 200) {
                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil !',
                    text: response.responseJSON._message,
                    confirmButtonColor: "#0D6EFD",
                    confirmButtonText: 'Baiklah',
                    onClose: function() {
                        window.location.href = BaseUrl + '/user/detail_surat/' + response.responseJSON._data;
                    }
                  })
            } else {
                console.log('error')
            }
        },
        dataType: 'json'
    })
});

function getSurat() {
    $.ajax({
        data: '',
        url: ServeUrl + "/user/get_surat",
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        complete: function(response) {
            console.log('response', response)
            if (response.responseJSON._status == 200) {

                var inc = 1;
                var html = '';
                var data = response.responseJSON._data;
                for (let i = 0; i < data.length; i++) {

                    if (data[i].jenis_surat == 'Surat Domisili') {
                        jumlahDomisili++;
                        
                    } else if (data[i].jenis_surat == 'Surat Keterangan Tidak Mampu') {
                        jumlahSktm++;

                    } else {
                        jumlahSkck++;

                    }
                    html += '<tr>';
                    html += '<td scope="row" class="text-center align-middle">'+inc+'</td>';
                    html += '<td style="text-align: left;" class="align-middle">'+data[i].jenis_surat+'</td>';
                    html += '<td scope="row" class="text-center align-middle">'+data[i].nama_lengkap+'</td>';
                    html += '<td class="text-center align-middle">'+formatDate(data[i].updated_at)+'</td>';

                    if (data[i].status == 'Diterima') {
                        html += '<td class="text-center align-middle"><span class="badge bg-success">'+data[i].status+'</span></td>';
                    } else if (data[i].status == 'Ditolak') {
                        html += '<td class="text-center align-middle"><span class="badge bg-danger">'+data[i].status+'</span></td>';
                    } else {
                        html += '<td class="text-center align-middle"><span class="badge bg-success">'+data[i].status+'</span></td>';
                    }

                    html += '<td class="text-center">';
                    html += '<button type="button" onclick="redDetail(`'+data[i].kode_surat+'`)" class="btn btn-primary btn-sm" style="margin-right: 10px">Detail</button>';
                    html += '<a href="'+ BaseUrl + '/user/update_surat/' + data[i].kode_surat +'" class="btn btn-info text-white btn-sm">Perbarui</a>';
                    html += '</td>';
                    html += '</tr>';
                    inc++;
                }
                $('#generateData').html(html);
                    $('#tables').DataTable(
                        {
                            "dom": '<"dt-buttons"Bf><"clear">lirtp',
                            "bInfo" : false,
                            "buttons": [
                                'colvis',
                                'copyHtml5',
                        'csvHtml5',
                                'excelHtml5',
                        'pdfHtml5',
                                'print'
                            ]
                        }
                    );

            } else {
                console.log('error')
            }
        },
        dataType: 'json'
    })
}
getSurat()

  function formatDate(date){
    let tanggal = moment(date, 'YYYY-MM-DD HH:mm:ss').format('DD-MMMM-YYYY');
    return tanggal;
  }

  function redDetail(kode) {
    window.location.href = BaseUrl + '/user/detail_surat/' + kode;
  }

  function validateFileType(attr){

    if(attr.files[0].size > 5000000){
        alert("File terlalu besar, Maksimal file adalah 5MB!");
        $('#' + attr.id).val('')
        return;
     };

    var fileName = document.getElementById(attr.id).value;
    var idxDot = fileName.lastIndexOf(".") + 1;
    var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
        
    }else{
        $('#' + attr.id).val('')
        alert("Hanya file jpg, jpeg dan png yang diperbolehkan !!");
    }
}

function handleChange(checkbox) {
    if(checkbox.checked == true){
        $('#submitSuratProses').fadeIn(1000)
    }else{
        $('#submitSuratProses').hide()
   }
}$('#submitSuratProses').hide()