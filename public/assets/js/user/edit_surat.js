function getDetail() {
    
    $.ajax({
        data: '',
        url: ServeUrl + "/user/detail_surat/" + window.location.pathname.split('/').pop(),
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        complete: function(response) {
            
            if (response.responseJSON._status == 200) {
                $('#kode_surat').val(response.responseJSON._data.kode_surat)
                $('#nama_lengkap').val(response.responseJSON._data.nama_lengkap)
                $('#jenis_surat').val(response.responseJSON._data.jenis_surat)
                $('#alamat').val(response.responseJSON._data.alamat)
                $('#kodeSurat').val(response.responseJSON._data.kode_surat)

                if (response.responseJSON._data.penghasilan == null) {
                    clearPenghasilan();
                } else {
                    $('#penghasilan').val(response.responseJSON._data.penghasilan)
                $('#pekerjaan').val(response.responseJSON._data.pekerjaan)
                }
               

            } else {
                console.log('error')
            }
        },
        dataType: 'json'
    })
}
getDetail();

function clearPenghasilan() {
    $('.hidden-form-input').hide();
}

function updateSurat() {
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var form = $('#update-surat')[0];
    var request = new FormData(form);
    request.append("_token", csrf_token );
    
    $.ajax({
        data: request,
        url: ServeUrl + '/user/update_surat',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
            startLoading();
        },
        complete: function() {
            stopLoading();
        },
        success: function(response) {
            Swal.fire({
                title: '<div style="color: white;">Berhasil!</div>',
                html: '<p class="card-text"><small class="text-muted">Perintah ini berhasil diproses. <br> Lakukan pengecekan ulang pada aksi ini.</small></p>',
                type: 'success',
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Ya, Baiklah!',
                confirmButtonClass: 'btn btn-second w-btn-100',
                background: '#14274E',
                cancelButtonClass: 'btn btn-danger ml-1',
                onClose: function() {
                    window.location.reload()
                }
            })
        },error: function(xhr) {
    
        },
    })
}
