// var ServeUrl = "http://127.0.0.1:8000/api";
// var BaseUrl = "http://127.0.0.1:8000";


var currentIDNotifikasi = 0;
function getNotifikasi() {
    $.ajax({
        url: ServeUrl + '/user/get_notifikasi',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        complete: function(response) {
            if (response.responseJSON._status == 200) {

                var data = response.responseJSON._data;

                if (data == 'tidak ada surat') {
                    
                } else {
                    currentIDNotifikasi = data.kode_surat
                    $('#notifikasi').modal('show')
                    $('#detailSurat').html('Pengajuan surat ' + data.jenis_surat + ' pada tanggal ' + formatDate(data.created_at) + ' telah <b>' + data.status)
                }

                
            } else {
                console.log("Something Wrong");
            }
        },error: function(xhr, status, error) {
            console.log("xhr", xhr);
            console.log("status", status);
            console.log("error", error);
        },
    })
}
getNotifikasi()

function redirectDetailSuratNotifikasi() {
    $.ajax({
        url: ServeUrl + '/user/notifikasi/' + currentIDNotifikasi,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'GET',
        dataType: 'json',
        complete: function(response) {
            if (response.responseJSON._status == 200) {
                window.location.href = BaseUrl + '/user/detail_surat/' + currentIDNotifikasi;
            } else {
                console.log("Something Wrong");
            }
        },error: function(xhr, status, error) {
            console.log("xhr", xhr);
            console.log("status", status);
            console.log("error", error);
        },
    })   
}

  function getPesanChat() {
      $.ajax({
          url: ServeUrl + '/user/get_pesan_chat',
          processData: false,
          contentType: false,
          cache: false,
          timeout: 600000,
          type: 'GET',
          dataType: 'json',
          complete: function(response) {
              if (response.responseJSON._status == 200) {
      
                var data = response.responseJSON._data;

                var html = '';
                for (let i = 0; i < data.chat.length; i++) {

                    if (data.user.id == data.chat[i].id_pengirim) {
                        html += '<div class="message user">';
                        html += '<div class="profile">';
                        html += '<img src="'+ BaseUrl + '/assets/image/data/' + data.user.picture +'" />';
                        html += '</div>';
                        html += '<div class="message-meta">';
                        html += '<p>'+data.chat[i].nama_pengirim+'</p>';
                        html += '<p></p>';
                        html += '</div>';
                        html += '<div class="message-content">';
                        html += '<p>'+data.chat[i].message+'</p>';
                        html += '</div>';
                        html += '</div>';
                    } else {
                        html += '<div class="message">';
                        html += '<div class="profile">';
                        html += '<img src="'+ BaseUrl + '/assets/image/data/default.png'+'" />';
                        html += '</div>';
                        html += '<div class="message-meta">';
                        html += '<p>Admin</p>';
                        html += '<p></p>';
                        html += '</div>';
                        html += '<div class="message-content">';
                        html += '<p>'+data.chat[i].message+'</p>';
                        html += '</div>';
                        html += '</div>';
                    }

                    $('#generateMessage').html(html);

                }


              } else {
                  console.log("Something Wrong");
              }
          },error: function(xhr, status, error) {
              console.log("xhr", xhr);
              console.log("status", status);
              console.log("error", error);
          },
      })
  }
  getPesanChat()
  

  $('.chat-header').click(function(){
    $(this).toggleClass('offline');
    $(this).toggleClass('online');
    $('.chat-window').toggleClass('docked');
  });
  
  setInterval(function(){
    $('.progress-indicator').toggleClass('hide');
  },7846);

  function kirimPesanChat() {
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
      var request = new FormData();
      request.append("message", $('#pesanChat').val().replaceAll("><", "-") );
      request.append("uniqid", makeRandomText(11) );
      request.append("_token", csrf_token );
      
      $.ajax({
          data: request,
          url: ServeUrl + '/user/kirim_chat',
          processData: false,
          contentType: false,
          cache: false,
          timeout: 600000,
          type: 'POST',
          dataType: 'json',
          complete: function(response) {
              if (response.responseJSON._status == 200) {
                $('#pesanChat').val('')
                getPesanChat()
              } else {
                  console.log("Something Wrong");
              }
          },error: function(xhr, status, error) {
              console.log("xhr", xhr);
              console.log("status", status);
              console.log("error", error);
          },
      })
  }

  function makeRandomText(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }
 
 function startLoading() {
    $.LoadingOverlay("show", {
        image : false,
        zIndex : 1000000,
        fade: [0, 0],
        size: 200,
        maxSize: 200,
        background: "#d6eaff52",
        fontawesome : "fa fa-cog fa-spin",
        imageAnimation: "2000ms rotate_right"
    });
}

function stopLoading() {
    $.LoadingOverlay("hide");
}

$(document).ajaxStop(function() {
    stopLoading()
});

$(document).ajaxStart(function(){ 
        startLoading();
});