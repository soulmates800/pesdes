
function openModal(card) {
    switch (card) {
        case 'dsl':
            $('#modalLanding').modal('show');
            $('#gambarKerja').attr('src', BaseUrl + '/assets/image/data/alur_domisili.PNG')
            $('#tittle-modal').html('Surat Domisili')
            break;
        case 'sktm':
            $('#modalLanding').modal('show');
            $('#gambarKerja').attr('src', BaseUrl + '/assets/image/data/alur_tidakmampu.PNG')
            $('#tittle-modal').html('Surat Keterangan Tidak Mampu')
            break;
        case 'skck':
            $('#modalLanding').modal('show');
            $('#gambarKerja').attr('src', BaseUrl + '/assets/image/data/alur_skck.PNG')
            $('#tittle-modal').html('Surat Keterangan Catatan Kepolisian')
            break;
        default:
            break;
    }
}