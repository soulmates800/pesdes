var csrf_token = $('meta[name="csrf-token"]').attr('content');
$("#authRegister").submit(function(event) {
    event.preventDefault();
    
    var form = $('#authRegister')[0];
    var data = new FormData(form);
    data.append("_token", csrf_token );
    $.ajax({
        data: data,
        url: ServeUrl + "/register",
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        type: 'POST',
        complete: function(response) {
            if (response.responseJSON._status == 200) {
                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil !',
                    text: response.responseJSON._message,
                    confirmButtonColor: "#0D6EFD",
                    confirmButtonText: 'Baiklah',
                    onClose: function() {
                        window.location.href = BaseUrl + '/login';
                    }
                  })
            } else {
                console.log('error')
            }
        },
        dataType: 'json'
    })
});

function redirectLogin() {
    window.location.href = BaseUrl + '/login'
}