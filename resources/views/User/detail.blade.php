@include('User.Layout.head')
<body style="background: #F1F3FF">

<div class="container-fluid">
<div class="row">
  
    @include('User.Layout.sidebar')

    <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
        
<div class="row container_input_surat rounded shadow-sm mt-3 ml-2 mr-2 mb-5" style="background: #fff">
    <div class="row g-3">
      
      <div class="col-3 pb-3 border_bottom">
        <span class="title_label">Kode Surat</span>
      </div>
      <div class="col-9 pb-3 border_bottom">
        <div id="dt_kodeSurat"></div>
      </div>

      <div class="col-3 pb-3 border_bottom">
        <span class="title_label">Jenis Surat</span>
      </div>
      <div class="col-9 pb-3 border_bottom">
        <div id="dt_jenis_surat"></div>
      </div>

      <div class="col-3 pb-3 border_bottom">
        <span class="title_label">Nama Lengkap</span>
      </div>
      <div class="col-9 pb-3 border_bottom">
        <div id="dt_nama_lengkap"></div>
      </div>

      <div class="col-3 pb-3 border_bottom">
        <span class="title_label">Alamat</span>
      </div>
      <div class="col-9 pb-3 border_bottom">
        <div id="dt_alamat"></div>
      </div>

      <div class="col-3 pb-3 border_bottom">
          <span class="title_label">Email</span>
      </div>
      <div class="col-9 pb-3 border_bottom">
          <div id="dt_email"></div>
      </div>

      <div class="col-3 pb-3 border_bottom">
        <span class="title_label">Nomor Telepon</span>
      </div>
      <div class="col-9 pb-3 border_bottom">
          <div id="dt_telepon"></div>
      </div>

      <div class="col-3 pb-3 border_bottom">
        <span class="title_label">Agama</span>
      </div>
      <div class="col-9 pb-3 border_bottom">
          <div id="dt_agama"></div>
      </div>

      <div class="col-3 pb-3 border_bottom">
        <span class="title_label">Jenis Kelamin</span>
      </div>
      <div class="col-9 pb-3 border_bottom">
          <div id="dt_jenis_kelamin"></div>
      </div>

      <div class="col-3 pb-3 border_bottom">
        <span class="title_label">Tempat / Tanggal Lahir</span>
      </div>
      <div class="col-9 pb-3 border_bottom">
          <div id="dt_tempat_tanggal_lahir"></div>
      </div>

      <div class="col-3 pb-3 border_bottom">
        <span class="title_label">NIK</span>
      </div>
      <div class="col-9 pb-3 border_bottom">
          <div id="dt_nik"></div>
      </div>
      <div class="col-3 pb-3 border_bottom">
        <span class="title_label">Nomor KTP</span>
      </div>
      <div class="col-9 pb-3 border_bottom">
          <div id="dt_no_ktp"></div>
      </div>

      <div class="col-3 pb-3 border_bottom">
        <span class="title_label">Foto Profile</span>
      </div>
      <div class="col-9 pb-3 border_bottom">
          <a id="foto-profile-light" href="" data-lightbox="image-1" data-title="Foto Profile">
            <img class="imageFcp" id="foto_profile" src=""/>
          </a>
      </div>
    </div>
    <div class="row g-3" id="generateData">
    </div>

    <div class="row g-3 pb-3 " id="generateData">
        <div class="col-3 pb-3 border_bottom">
            <span class="title_label">Status</span>
          </div>
          <div class="col-9 pb-3 border_bottom">
            <div id="dt_status"></div>
          </div>
    </div>

    <div class="col-3 pb-3 border_bottom diterima-surat">
      <span class="title_label">Tanggal Diambil</span>
    </div>
    <div class="col-9 pb-3 border_bottom diterima-surat">
        <div id="dt_tanggal_diambil"></div>
    </div>

    <div class="col-3 pb-3 pt-3 border_bottom diterima-surat">
      <span class="title_label">Pengambil Surat</span>
    </div>
    <div class="col-9 pb-3 pt-3 border_bottom diterima-surat">
        <div id="dt_pengambil"></div>
    </div>

    <div class="col-3 pb-3 border_bottom ditolak-surat">
      <span class="title_label">Alasan Ditolak</span>
    </div>
    <div class="col-9 pb-3 border_bottom ditolak-surat">
        <div id="dt_alasan_ditolak"></div>
    </div>

    <div class="row g-3" id="generateData">
        <div class="col-12 pb-3 border_bottom">
            <button onclick="back()" type="button" class="btn btn-primary btn-sm">Kembali</button>
          </div>
    </div>
</div>



</div>
</div>

@include('User.Layout.footer')
<script src="{{url('assets/js')}}/user/detail.js"></script>
