@include('User.Layout.head')
<body style="background: #F1F3FF">

<div class="container-fluid">
    <div class="row">
    @include('User.Layout.sidebar')
    <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4"> 
    <div class="row container_input_surat rounded shadow-sm mt-3 ml-2 mr-2 mb-5" style="background: #fff">
        <form id="update-surat" class="mt-5">
            <div class="row g-3">
                <div class="col-3 pb-3 border_bottom">
                    <span class="title_label">Kode Surat</span>
                </div>

                <div class="col-9 pb-3 border_bottom">
                    <input type="text" class="form-control" id="kode_surat" name="kode_surat" readonly>
                </div>

                <div class="col-3 pb-3 border_bottom">
                    <span class="title_label">Nama Lengkap</span>
                </div>

                <div class="col-9 pb-3 border_bottom">
                    <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" readonly>
                </div>

                <div class="col-3 pb-3 border_bottom">
                    <span class="title_label">Jenis Surat</span>
                </div>

                <div class="col-9 pb-3 border_bottom">
                    <input type="text" class="form-control" id="jenis_surat" name="jenis_surat" readonly>
                </div>

                <div class="col-3 pb-3 border_bottom">
                    <span class="title_label">Alamat</span>
                </div>

                <div class="col-9 pb-3 border_bottom">
                    <input type="text" class="form-control" id="alamat" name="alamat">
                </div>

                <div class="col-3 pb-3 border_bottom hidden-form-input">
                    <span class="title_label">Penghasilan</span>
                </div>

                <div class="col-9 pb-3 border_bottom hidden-form-input">
                    <input type="text" class="form-control" id="penghasilan" name="penghasilan">
                </div>
                
                <div class="col-3 pb-3 border_bottom hidden-form-input">
                    <span class="title_label">Pekerjaan</span>
                </div>

                <div class="col-9 pb-3 border_bottom hidden-form-input">
                    <input type="text" class="form-control" id="pekerjaan" name="pekerjaan">
                </div>

                <div class="col-md-12">
                    <div class="alert alert-warning" role="alert">
                        Beberapa informasi mungkin tidak dapat diubah. Anda bisa mengajukan surat kembali
                    </div>
                </div>
                <div class="col-12 pb-3 border_bottom">
                    <button onclick="updateSurat()" type="button" class="btn btn-primary btn-sm">Perbarui Surat</button>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>

@include('User.Layout.footer')
<script src="{{url('assets/js')}}/user/edit_surat.js"></script>
