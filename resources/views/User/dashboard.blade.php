@include('User.Layout.head')
<body style="background: #F1F3FF">

  <div class="container-fluid mb-5">
  <div class="row">
    
      @include('User.Layout.sidebar')
  
    <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4 mt-4">
        <div class="card-list">
          <div class="row mb-4">
            <div class="col-md-12">
              <div class="container_banner_user_panel">
              <img src="{{ url('assets/image/slider/banner_user-01.png') }}"/>
            </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-md-6 col-lg-4 col-xl-3 mb-2">
              <div class="card" style="background: #4764E6">
                <div class="title">Total Surat</div>
                <i class="zmdi zmdi-upload"></i>
                <div class="value" id="totalSurat"></div>
                <div class="stat"><b>13</b>% increase</div>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 col-xl-3 mb-2">
              <div class="card blue">
                <div class="title">Surat Diproses</div>
                <i class="zmdi zmdi-upload"></i>
                <div class="value" id="diproses"></div>
                <div class="stat"><b>4</b>% increase</div>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 col-xl-3 mb-2">
              <div class="card green">
                <div class="title">Surat Diterima</div>
                <i class="zmdi zmdi-download"></i>
                <div class="value" id="diterima"></div>
                <div class="stat"><b>13</b>% decrease</div>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 col-xl-3 mb-2">
              <div class="card red">
                <div class="title">Surat Ditolak</div>
                <i class="zmdi zmdi-download"></i>
                <div class="value" id="ditolak"></div>
                <div class="stat"><b>13</b>% decrease</div>
              </div>
            </div>
          </div>

          <div class="row">
          <div class="col-md-8  mb-3">
            <div class="card aktivitas shadow mb-4">
            <h6 class="border-bottom pb-2 mb-0">Aktifitas Terbaru</h6>
            <div id="generateLogData"></div>

          </div>
        </div>
        <div class="col-xl-4 col-lg-5">
          <div class="card shadow mb-3">
              <div class="single-cards-item">
                <div class="single-product-image">
                    <a href="{{ url('user/profile') }}" alt=""></a>
                </div>
                <div class="single-product-text">
                    <img src="{{ url('assets/image/data') .'/'. $data['user_data']->picture}}" alt="">
                    <h4><a class="cards-hd-dn" href="{{ url('user/profile') }}">{{$data['user_data']->name}}</a></h4>
                    <h5>{{$data['user_data']->roles}}</h5>
                </div>
            </div>
          </div>
      </div>
    </div>
        </div>
    </main>
  </div>
  </div>

  <div class="footer_user_panel mt-5">
    <p><i class="far fa-copyright" style="margin-right: 5px"></i>PESDES - Pengajuan Surat Desa - Gayu Gumelar</p>
  </div>
@include('User.Layout.footer')
<script src="{{url('assets/js')}}/user/dashboard.js"></script>
<script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js" integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha" crossorigin="anonymous"></script>
