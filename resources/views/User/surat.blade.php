@include('User.Layout.head')
<body style="background: #F1F3FF">
    <div class="container-fluid">
        <div class="row">
            @include('User.Layout.sidebar')
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="row">
                    <div class="col-md-12 mt-3">
                        <div class="card text-white" style="background: #4764E6">
                            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom mt-3">
                                <h3 class="h5">Buat Surat / Dokumen</h3>
                                <div class="btn-toolbar mb-2 mb-md-0">
                                    <button type="button" class="btn btn-light btn-sm create_mail"
                                        data-bs-toggle="modal" data-bs-target="#testModal">Ajukan Pembuatan
                                        Surat</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mt-3 mb-5">
                        <div class="card p-4">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover dt-responsive">
                                  <thead class="text-center">
                                    <tr>
                                        <th>#</th>
                                        <th style="text-align: left">Jenis Surat</th>
                                        <th>Nama Pengirim</th>
                                        <th>Tanggal Dibuat</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                  </thead>
                                  <tbody id="generateData">
                
                                  </tbody>
                                </table>
                              </div>
                            
                        </div>
                    </div>
                </div>
                <div class="footer_user_panel">
                    <p><i class="far fa-copyright" style="margin-right: 5px"></i>PESDES - Pengajuan Surat Desa - Gayu
                        Gumelar</p>
                </div>

            </main>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="testModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Pengajuan Surat</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form class="modal-body" id="buatSurat">
                    {{ csrf_field() }}
                    <div class="row container_input_surat" style="border-top: 1px solid #ebebeb">


                        <div class="row g-3">
                            <div class="col-md-6 pb-3 border_bottom">
                                <span id="kodeSurats"></span>
                            </div>
                            <div class="col-12 pb-3 border_bottom">
                                <label class="form-label">Jenis Surat</label>
                                <select id="jenis_surat" name="jenis_surat" required onchange="handleJenisSurat()"
                                    class="form-select" aria-label="Default select example">
                                    <option selected disabled>Pilih jenis surat</option>
                                    <option value="Surat Domisili">Pembuatan Surat Domisili</option>
                                    <option value="Surat Keterangan Tidak Mampu">Pembuatan Surat Keterangan Tidak Mampu
                                    </option>
                                    <option value="Surat Keterangan Catatan Kepolisian">Pembuatan Surat Keterangan
                                        Catatan Kepolisian</option>
                                </select>
                            </div>
                        </div>

                        <div id="generateJenisSurat" class="row g-3 pt-3"></div>

                        <div class="col-12 pt-3">
                            <div class="form-check">
                                <input class="form-check-input check-surat" type="checkbox" id="gridCheck" onchange='handleChange(this);'>
                                <label class="form-check-label check-surat" for="gridCheck">
                                    * Saya menyatakan bahwa semua data yang di inputkan adalah benar dan harap perhatikan isian data anda
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="submitSuratProses" class="btn btn-primary">Buat Surat</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('User.Layout.footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"
        integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ=="
        crossorigin="anonymous"></script>
    <script src="{{ url('assets/js') }}/user/surat.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js"
        integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"
        integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha" crossorigin="anonymous">
    </script>
