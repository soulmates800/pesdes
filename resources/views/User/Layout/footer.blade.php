<div class="footer_user_panel">
  <p><i class="far fa-copyright" style="margin-right: 5px"></i>PESDES - Pengajuan Surat Desa - Gayu Gumelar</p>
</div>

<section class="chat-window docked">
  <div class="chat-header offline">
    <p>Live Chat</p>
    <span class="close"></span>
  </div>
  <div class="chat-body">
    <div class="message-container" id="generateMessage">

    </div>
  </div>
  <div class="chat-footer">
    <div class="progress-indicator hide">
      
    </div>
    <div class="form-area">
      <input id="pesanChat" type="text" />
      <button onclick="kirimPesanChat()"></button>
    </div>
  </div>
</section>

<div class="modal fade" id="notifikasi" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Informasi !!</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <p id="detailSurat"></p>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="redirectDetailSuratNotifikasi()" class="btn btn-primary">Lihat Detail Surat</button>
        </div>
      </div>
    </div>
  </div>

<script src="{{url('assets/js')}}/jquery.min.js"></script>
<script src="{{url('assets/js')}}/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>

<script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.js"></script>
<script src="https://unpkg.com/filepond/dist/filepond.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/filepond-plugin-file-validate-type@1.2.6/dist/filepond-plugin-file-validate-type.min.js" integrity="sha256-JgxBK0TrBA7h/OF6bnbWS0uTIhSzlmi/zWiz634s7Os=" crossorigin="anonymous"></script>
<script src="https://unpkg.com/jquery-filepond/filepond.jquery.js"></script>

<script src="{{url('assets/js')}}/lightbox.js"></script>
<script>
  var BaseUrl = "{{env('APP_SERVER_BASE')}}";
  var ServeUrl = "{{env('APP_SERVER_API')}}";
</script>
<script src="{{url('assets/js')}}/global.js"></script>