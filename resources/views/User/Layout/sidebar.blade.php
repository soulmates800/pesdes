
<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block sidebar collapse">
    <div class="position-sticky pt-3">
      <ul class="nav flex-column">
        <li class="nav-item mt-1 mb-3" style="border-bottom: 1px solid #ebebeb">
          <a class="nav-link" href="{{ url('user/home') }}">
            <span data-feather="file"></span>
            <i class="fas fa-address-book" style="margin-right: 10px"></i>Dashboard
          </a>
        </li>
        <li class="nav-item mt-1 mb-1" style="border-bottom: 1px solid #ebebeb">
          <a class="nav-link" href="{{ url('user/surat') }}">
            <span data-feather="file"></span>
            <i class="fas fa-plus-circle" style="margin-right: 10px"></i>Buat Surat
          </a>
        </li>
        <li class="nav-item mt-1 mb-1" style="border-bottom: 1px solid #ebebeb">
          <a class="nav-link" href="{{ url('user/profile') }}">
            <span data-feather="shopping-cart"></span>
            <i class="fas fa-user-circle" style="margin-right: 10px"></i>Profile
          </a>
        </li>
        <form id="logoutUser" method="POST" action="{{ route('logout') }}" >
          @csrf
        <li onclick="event.preventDefault(); this.closest('form').submit();" class="nav-item mt-3" style="border-bottom: 1px solid #ebebeb;">
            <a class="nav-link" href="#">
              <span data-feather="shopping-cart"></span>
              <i class="fas fa-sign-out-alt" style="margin-right: 10px"></i>
             
            Logout
            </a>
          </li>
        </form>
      </ul>

      
    
    </div>
  </nav>