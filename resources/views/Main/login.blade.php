{{-- @include('Main.Layout.head') --}}

<!DOCTYPE html>
<html lang="en" style="height: 100% !important;">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pesdes - Pengajuan Surat Digital</title>
    <link href="{{ url('assets/css') }}/login.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@600&display=swap" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body class="form-v10">
  <div class="page-content">
      <div class="form-v10-content">
          <form class="form-detail" id="authLogin">
              <div class="form-right shadow">
                  <h2>Login</h2>
                  <div class="form-row">
                      <input type="email" name="email" class="street" id="email" placeholder="Email" required>
                  </div>
                  <div class="form-row">
                      <input type="password" name="password" class="additional" id="password" placeholder="Password" required>
                  </div>
                  <div class="form-row-last">
                    <a class="text-white belum-daftar" href="{{ url('lupa-password') }}" name="register">Lupa Password ?</a>
                </div>
                  <div class="form-row-last">
                      <input type="button" onclick="loginAuth()" class="register" value="Masuk">
                      <input type="button" onclick="redirectRegister()" class="text-link-auth" value="Belum Daftar?">
                  </div>
                  
              </div>
          </form>
      </div>
  </div>
</body>

<script src="{{ url('assets/js') }}/jquery.min.js"></script>
<script src="{{ url('assets/js') }}/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script type="text/javascript">
  var BaseUrl = "{{ env('APP_SERVER_BASE') }}";
  var ServeUrl = "{{ env('APP_SERVER_API') }}";
</script>
<script src="{{ url('assets/js') }}/login.js"></script>