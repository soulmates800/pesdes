{{-- @include('Main.Layout.head') --}}
{{-- <div style="background: #4764E6; width: 100%; height: 100%; !important; position: absolute;"></div> --}}
{{-- <div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="body_login">
            <!--<img class="bg" src="{{ url('assets/image/login/background.jpg') }}"/>-->

            <div class="container_card_main_login">
                <div class="row">
                    <div class="col-md-12 card_col_landing mt-5">

                        <div class="container_card_landing">
                      <div class="card_costum_login">
                          <div class="container_form_login">
                        <form id="authRegister">
                            <div class="mb-3 mt-2">
                                <div class="row">
                                    <div class="col-md-3 divider_login"></div>
                                    <div class="col-md-6 text-center"><b>Daftar Akun</b></div>
                                    <div class="col-md-3 divider_login"></div>
                                </div>
                            </div>
                            <div class="mb-3">
                              <input placeholder="Nama" name="name" type="text" class="form-control">
                            </div>
                            <div class="mb-3">
                              <input name="email" placeholder="Email" type="email" class="form-control">
                            </div>
                            <div class="mb-3">
                                <input name="password" placeholder="Password" type="password" class="form-control">
                            </div>
                            
                            <div class="mb-3">
                              <input placeholder="Alamat" name="name" type="text" class="form-control">
                            </div>
                            <div class="mb-3">
                              <input placeholder="Agama" name="name" type="text" class="form-control">
                            </div>
                            <div class="mb-3">
                              <input placeholder="Jenis Kelamin" name="name" type="text" class="form-control">
                            </div>
                            <div class="mb-3">
                              <input placeholder="Tempat Tanggal lahir" name="name" type="text" class="form-control">
                            </div>
                            <div class="mb-3">
                              <input placeholder="Nama" name="name" type="text" class="form-control">
                            </div>
                            <div class="mb-3">
                              <input placeholder="Nama" name="name" type="text" class="form-control">
                            </div>
                            <div class="mb-3">
                              <input placeholder="Nama" name="name" type="text" class="form-control">
                            </div>
                            <button type="submit" class="btn_login_costum">Daftar Sekarang</button>
                            <a href="{{url('/login')}}" class="btn_register_red_costum">Sudah memiliki akun?</a>
                          </form>
                        </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>

        </div>

        </div>
    </div>
</div> --}}

<!DOCTYPE html>
<html lang="en" style="height: 100% !important;">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pesdes - Pengajuan Surat Digital</title>
    <link href="{{ url('assets/css') }}/register.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@600&display=swap" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body class="form-v10">
    <div class="page-content">
        <div class="form-v10-content">
            <form class="form-detail" id="authRegister">
                <div class="form-right shadow">
                    <h2>Registrasi</h2>
                    <div class="form-row">
                        <input type="text" name="name" class="street" id="nama" placeholder="Nama" required>
                    </div>
                    <div class="form-row">
                        <input type="text" name="alamat" class="additional" id="alamat" placeholder="Alamat" required>
                    </div>
                    <div class="form-group">
                        <div class="form-row form-row-1">
                            <input type="text" name="telepon" class="code" id="telepon" placeholder="Nomor Telepon" required>
                        </div>
                        <div class="form-row form-row-2">
                            <input type="text" name="tempat_tanggal_lahir" class="phone" id="tempat_tanggal_lahir" placeholder="Tempat / Tanggal Lahir" required>
                        </div>
                    </div>
                    <div class="form-group">
                      <div class="form-row form-row-1">
                        <input type="text" name="agama" class="phone" id="agama" placeholder="Agama" required>
                    </div>
                      <div class="form-row form-row-2">
                        <select name="jenis_kelamin">
                            <option selected disabled>Jenis Kelamin</option>
                            <option value="Laki Laki">Laki Laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                        <span class="select-btn">
                            <i class="fas fa-arrow-circle-down"></i>
                        </span>
                    </div>
                  </div>
                    <div class="form-row">
                        <input type="text" name="no_ktp" class="additional" id="no_ktp" placeholder="Nomor KTP" required>
                    </div>
                    <div class="form-row">
                        <input type="text" name="nik" class="additional" id="nik" placeholder="NIK (Nomor Induk Kependudukan)" required>
                    </div>
                    <div class="form-group">
                        <div class="form-row form-row-1">
                            <input type="email" name="email" class="code" id="email" placeholder="Email" required>
                        </div>
                        <div class="form-row form-row-2">
                            <input type="password" name="password" class="phone" id="password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="form-row-last">
                        <input type="submit" name="register" class="register" value="Daftar">
                        <input type="button" onclick="redirectLogin()" class="text-link-auth" value="Sudah Daftar?">
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>

<script src="{{ url('assets/js') }}/jquery.min.js"></script>
<script src="{{ url('assets/js') }}/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script type="text/javascript">
    var BaseUrl = "{{ env('APP_SERVER_BASE') }}";
    var ServeUrl = "{{ env('APP_SERVER_API') }}";
</script>
<script src="{{ url('assets/js') }}/register.js"></script>