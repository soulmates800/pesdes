

@include('Main.Layout.head')


<body>

    <div class="desktop-navbar">
        <div class="row warna-navbar">
            <div class="col-md-6 container_logos">
                <img src="{{ url('assets/image/logo/logo.png') }}" class="logo" alt="...">
            </div>
            <div class="col-md-6 container_logos">
                <a class="link_navbar_costum" href="{{ url('/') }}">HOME</a>
                <a class="link_navbar_costum" href="{{ url('/tentang') }}">TENTANG DESA</a>
                <a class="link_navbar_costum_login login_button" href="{{ url('/login') }}">LOGIN</a>
            </div>
        </div>
    </div>

  <div class="col-md-12 desktop-navbar">
      <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
          <div class="carousel-inner">
              <div class="container_gradient_slider"></div>
              <div class="carousel-item active">
                  <img src="{{ url('assets/image/banner/banner-gayu-1.jpg') }}" class="d-block w-100 costum_slider" alt="...">
              </div>
              <div class="carousel-item">
                  <img src="{{ url('assets/image/banner/banner-gayu-2.jpg') }}" class="d-block w-100 costum_slider" alt="...">
              </div>
              <div class="carousel-item">
                  <img src="{{ url('assets/image/banner/banner-gayu-3.jpg') }}" class="d-block w-100 costum_slider" alt="...">
              </div>
              <div class="carousel-item">
                  <img src="{{ url('assets/image/banner/banner-gayu-4.jpg') }}" class="d-block w-100 costum_slider" alt="...">
              </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
          </a>
      </div>
  </div>

  <div class="col-md-12 mobile-navbar">
    <nav class="navbar navbar-expand-lg navbar-light bg-light ">
        <div class="container-fluid mt-3">
            <a class="navbar-brand" href="#">Pesdes</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <div class="row mt-3 border-top pt-3">
                    <div class="col-md-12">
                        <a class="nav-link active" style="text-decoration: none; color: black" aria-current="page" href="{{ url('/') }}">HOME</a>
                    </div>
                    <div class="col-md-12">
                        <a class="nav-link active" aria-current="page" style="text-decoration: none; color: black" href="{{ url('/tentang') }}">TENTANG DESA</a>
                    </div>
                    <div class="col-md-12">
                        <a class="nav-link active btn btn-outline-info" style="color: white" aria-current="page" href="{{ url('/login') }}">LOGIN</a>
                    </div>
                </div>
            </div>
        </div>
    </nav>
  </div>

  <div class="row m-5">
      <div class="col-md-12 text-center p-4 border">
          <img src="{{ url('assets/image/data/struktur_organisasi_desa.PNG') }}" class="image-sturktur" alt="" />
      </div>
  </div>
  
    <div class="row m-5">
        <div class="col-md-12 text-center">
            <h1><b>Berita Terbaru</b></h1>
        </div>
    </div>

    <div class="container">
      <div class="row">
          <div class="col-md-4 card_col_landing">
            <div class="card">
                <img src="{{ url('assets/image/landing/content-1.png') }}" class="card-img-top" alt="...">
                <div class="card-body p-1">
                  <h5 class="card-title mt-3 mb-3" style="color: black; font-size: 16px;">Sabtu 10 Febuari 2018 Desa Sungai Langka diresmikan menjadi desa agrowisata pertama di Provinsi Lampung.</h5>
                  <p class="card-text text-muted" style="color: black;">Acara ini diresmikan langsung oleh Gubernur Lampung Ridho Ficardo di resmikannya Sungai Langka sebagai Desa Agrowisata di Lampung, semoga hal ini dapat membawa kesejahtraan bagi masyarakat Sungai Langka dan membawa kemajuan bagi Provinsi Lampung khusuhnya Kabupaten Pesawaran.
                    Dalam sambutannya Gubernur Lampung ....</p>
                  <a target="_blank" href="https://potensi.pesawarankab.go.id/2018/09/17/sabtu-10-febuari-2018-desa-sungai-langka-diresmikan-menjadi-desa-agrowisata-pertama-di-provinsi-lampung/" class="btn btn-primary">Read More</a>
                </div>
              </div>
          </div>

          <div class="col-md-4 card_col_landing">
            <div class="card">
                <img src="{{ url('assets/image/landing/content-2-01.png') }}" class="card-img-top" alt="...">
                <div class="card-body p-1">
                  <h5 class="card-title mt-3 mb-3" style="color: black; font-size: 16px;">Jeruk BW Khas Sungai Langka</h5>
                  <p class="card-text text-muted" style="color: black;">Komoditi jeruk di Desa Sungai Langka memang tergolong baru dan belum memiliki cakupan perkebunan yang luas seperti komoditi lainnya yang lebih dulu dibudayakan ....</p>
                  <a target="_blank" href="https://potensi.pesawarankab.go.id/2018/09/17/jeruk-bw-khas-sungai-langka/" class="btn btn-primary">Read More</a>
                </div>
              </div>
          </div>

          <div class="col-md-4 card_col_landing">
            <div class="card">
                <img src="{{ url('assets/image/landing/content-3.png') }}" class="card-img-top" alt="...">
                <div class="card-body p-1">
                  <h5 class="card-title mt-3 mb-3" style="color: black; font-size: 16px;">Sungai Langka Prioritaskan Program Desa Wisata</h5>
                  <p class="card-text text-muted" style="color: black;">Program desa pariwisata tengah menjadi prioritas Desa Sungai Langka Kecamatan Gedongtataan, khususnya di Dusun VIII. Pembangunan tempat wisata di Desa Sungai Langka saat ini tengah gencar dilakukan dengan cara bergotong royong bersama masyarakat.
                    Sebagai bagian bidang ekonomi kreatif, Kepala Dusun VIII Desa Sungai Langka, Prio mengaku sudah mengerahkan masyarakatnya untuk bergotong-royong membersihkan sungai yang akan dijadikan tempat wisata. ....</p>
                  <a target="_blank" href="https://www.radarlamsel.com/sungai-langka-prioritaskan-program-desa-wisata/" class="btn btn-primary">Read More</a>
                </div>
              </div>
          </div>

      </div>
    </div>

    <div class="footer_landing mt-5">
        <div class="footer_padding_landing">
        <div class="row">

            <div class="col-md-8">
                <div class="title_footer_landing">TENTANG</div>
                <div class="content_tentang_landing">PESDES adalah sebuah platform pelayanan publik yang berfokus terhadap pelayanan pengajuan surat didesa.</div>
                <div class="container_sosial_landing">
                {{-- <a href="" style="text-decoration: none">
                    <span style="font-size: 1.5em; color: white;">
                    <i class="fab fa-facebook-square"></i>
                  </span></a>
                  <a href="" style="text-decoration: none">
                  <span style="font-size: 1.5em; color: white;">
                    <i class="fab fa-instagram-square"></i>
                  </span>
                </a> --}}
                </div>
            </div>

           

            <div class="col-md-4">
                <div class="title_footer_landing">LAYANAN</div>
                <div class="row">
               
                <div class="container_layanan_side">
                    <a href=""  style="text-decoration: none">
                        <i class="fas fa-phone icon_layanan_landing"><span class="layanan_footer_landing">0896-6564-6443</span></i>
                    </a>
                </div>

                <div class="container_layanan_side">
                    <a href=""  style="text-decoration: none">
                        <i class="fas fa-envelope icon_layanan_landing"><span class="layanan_footer_landing">gayugumelar@gmail.com</span></i>
                    </a>
                </div>

            </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12 pt-5">
                <div class="copyright_footer_landing"><i class="far fa-copyright" style="margin-right: 5px"></i>PESDES - Pengajuan Surat Desa - Gayu Gumelar</div>
            </div>
        </div>

    </div>
    </div>

</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</html>