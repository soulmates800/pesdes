

@include('Main.Layout.head')


<body>

    <div class="desktop-navbar">
        <div class="row warna-navbar">
            <div class="col-md-6 container_logos">
                <img src="{{ url('assets/image/logo/logo.png') }}" class="logo" alt="...">
            </div>
            <div class="col-md-6 container_logos">
                <a class="link_navbar_costum" href="{{ url('/') }}">HOME</a>
                <a class="link_navbar_costum" href="{{ url('/tentang') }}">TENTANG DESA</a>
                <a class="link_navbar_costum_login login_button" href="{{ url('/login') }}">LOGIN</a>
            </div>
        </div>
    </div>
    

  <div class="col-md-12 desktop-navbar">
      <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
          <div class="carousel-inner">
              <div class="container_gradient_slider"></div>
              <div class="carousel-item active">
                  <img src="{{ url('assets/image/banner/banner-gayu-1.jpg') }}" class="d-block w-100 costum_slider" alt="...">
              </div>
              <div class="carousel-item">
                  <img src="{{ url('assets/image/banner/banner-gayu-2.jpg') }}" class="d-block w-100 costum_slider" alt="...">
              </div>
              <div class="carousel-item">
                  <img src="{{ url('assets/image/banner/banner-gayu-3.jpg') }}" class="d-block w-100 costum_slider" alt="...">
              </div>
              <div class="carousel-item">
                  <img src="{{ url('assets/image/banner/banner-gayu-4.jpg') }}" class="d-block w-100 costum_slider" alt="...">
              </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
          </a>
      </div>
  </div>


  <div class="col-md-12 mobile-navbar">
    <nav class="navbar navbar-expand-lg navbar-light bg-light ">
        <div class="container-fluid mt-3">
            <a class="navbar-brand" href="#">Pesdes</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <div class="row mt-3 border-top pt-3">
                    <div class="col-md-12">
                        <a class="nav-link active" style="text-decoration: none; color: black" aria-current="page" href="{{ url('/') }}">HOME</a>
                    </div>
                    <div class="col-md-12">
                        <a class="nav-link active" aria-current="page" style="text-decoration: none; color: black" href="{{ url('/tentang') }}">TENTANG DESA</a>
                    </div>
                    <div class="col-md-12">
                        <a class="nav-link active btn btn-outline-info" style="color: white" aria-current="page" href="{{ url('/login') }}">LOGIN</a>
                    </div>
                </div>
            </div>
        </div>
    </nav>
  </div>
  


    <div class="row m-5">
        <div class="col-md-12 text-center">
            <h1><b>Cara Pengajuan Surat</b></h1>
        </div>
    </div>

    <div class="container">
      <div class="row">
          <div class="col-md-4 card_col_landing" onclick="openModal('dsl')">
            <div class="costum-center">
              <div class="container_card_landing">
                <div class="card_costum_landing">
                    <img src="{{ url('assets/image/landing/icon-kotak3.png') }}" alt="Avatar" class="background_card_landing p-5">
                    <div class="container_costum_landing">
                      <h4 class="text-center card_title_costum_landing"><b>PENGAJUAN SURAT DOMISILI</b></h4> 
                      <p class="deskripsi_card_landing pt-3">Kamu dapat mengajukan surat domisili diwebsite ini. 
                        Dengan mengunggah : Foto Kartu Keluarga (KK), Foto Kartu Tanda Penduduk (KTP).</p> 
                    </div>
                  </div>
                </div>
            </div>
          </div>

          <div class="col-md-4 card_col_landing">
            <div class="costum-center">
            <div class="container_card_landing" onclick="openModal('sktm')">
          <div class="card_costum_landing">
            <img src="{{ url('assets/image/landing/icon-kotak3.png') }}" alt="Avatar" class="background_card_landing p-5">
              <div class="container_costum_landing">
                <h4 class="text-center card_title_costum_landing"><b>PENGAJUAN SURAT KETERANGAN TIDAK MAMPU</b></h4> 
                <p class="deskripsi_card_landing pt-3">Kamu dapat mengajukan surat keterangan tidak mampu diwebsite ini. 
                  Dengan mengunggah : Foto Kartu Keluar (KK), Foto Kartu Tanda Penduduk (KTP), Foto Jaringan Pengaman Sosial (JPS).</p> 
              </div>
            </div>
          </div>
            </div>
        </div>

        <div class="col-md-4 card_col_landing">
          <div class="costum-center">
            <div class="container_card_landing" onclick="openModal('skck')">
          <div class="card_costum_landing">
            <img src="{{ url('assets/image/landing/icon-kotak3.png') }}" alt="Avatar" class="background_card_landing p-5">
              <div class="container_costum_landing">
                <h4 class="text-center card_title_costum_landing"><b>PENGAJUAN SURAT PENGANTAR SKCK</b></h4> 
                <p class="deskripsi_card_landing pt-3">Kamu dapat mengajukan surat pengantar SKCK diwebsite ini. 
                  Dengan mengunggah : Foto Kartu Keluarga (KK), Foto Kartu Tanda Penduduk (KTP Ayah-Ibu)</p> 
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>

    <div class="footer_landing mt-5">
        <div class="footer_padding_landing">
        <div class="row">

            <div class="col-md-8">
                <div class="title_footer_landing">TENTANG</div>
                <div class="content_tentang_landing">PESDES adalah sebuah platform pelayanan publik
                  yang berfokus terhadap pelayanan pengajuan surat didesa.</div>
                <div class="container_sosial_landing">
                </div>
            </div>

           

            <div class="col-md-4">
                <div class="title_footer_landing">LAYANAN</div>
                <div class="row">
               
                <div class="container_layanan_side">
                    <a href=""  style="text-decoration: none">
                        <i class="fas fa-phone icon_layanan_landing"><span class="layanan_footer_landing">0896-6564-6443</span></i>
                    </a>
                </div>

                <div class="container_layanan_side">
                    <a href=""  style="text-decoration: none">
                        <i class="fas fa-envelope icon_layanan_landing"><span class="layanan_footer_landing">gayugumelar@gmail.com</span></i>
                    </a>
                </div>

            </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12 pt-5">
                <div class="copyright_footer_landing"><i class="far fa-copyright" style="margin-right: 5px"></i>PESDES - Pengajuan Surat Desa - Gayu Gumelar</div>
            </div>
        </div>

    </div>
    </div>

    <div class="modal" id="modalLanding" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="tittle-modal"></h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body text-center">
            <div class="row">
              <div class="col-md-12">
                <img src="" alt="" id="gambarKerja" />
              </div>
              <div class="col-md-12">
                <a class="btn btn-info" href=" {{ url('register') }} ">Ajukan Surat Sekarang</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

</body>

<script src="{{url('assets/js')}}/jquery.min.js"></script>
<script src="{{url('assets/js')}}/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<script type="text/javascript">
  var BaseUrl = "{{env('APP_SERVER_BASE')}}";
  var ServeUrl = "{{env('APP_SERVER_API')}}";
</script>
<script src="{{ url('assets/js') }}/landing.js"></script>
</html>