@include('Admin.Layout.head')

<body style="background: #F1F3FF">

    <div class="container-fluid">
        <div class="row">

            @include('Admin.Layout.sidebar')
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4 mt-4">
                <div class="card-list">
                    <div class="row mb-4">
                        <div class="col-md-9">
                            <div class="container_banner_user_panel">
                                <img style="width: 100%" src="{{ url('assets/image/slider/slide2.png') }}" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <div class="single-cards-item">
                                <div class="single-product-image">
                                    <a href="#"><img src="{{$data['user_data']->picture}}" alt=""></a>
                                </div>
                                <div class="single-product-text">
                                    <img src="{{ url('assets/image/data') .'/'. $data['user_data']->picture}}" alt="">
                                    <h4><a class="cards-hd-dn" href="#">{{$data['user_data']->name}}</a></h4>
                                    <h5>{{$data['user_data']->roles}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="card" style="background: #4764E6">
                                <div class="title">Total Surat</div>
                                <i class="zmdi zmdi-upload"></i>
                                <div class="value">{{ $data['semua'] }}</div>
                                <div class="stat"><b>13</b>% increase</div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="card blue">
                                <div class="title">Surat Diproses</div>
                                <i class="zmdi zmdi-upload"></i>
                                <div class="value">{{ $data['diproses'] }}</div>
                                <div class="stat"><b>4</b>% increase</div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="card green">
                                <div class="title">Surat Diterima</div>
                                <i class="zmdi zmdi-download"></i>
                                <div class="value">{{ $data['diterima'] }}</div>
                                <div class="stat"><b>13</b>% decrease</div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="card red">
                                <div class="title">Surat Ditolak</div>
                                <i class="zmdi zmdi-download"></i>
                                <div class="value">{{ $data['ditolak'] }}</div>
                                <div class="stat"><b>13</b>% decrease</div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                    {{-- {{dd($data['surat'][0])}}
                    <div class="col-md-6 pb-5 mb-5" >
                        <div class="my-3 p-3 bg-white rounded shadow-sm">
                            <h6 class="border-bottom pb-2 mb-0">Aktifitas Surat Terbaru</h6>
                            @for ($i = 0; $i < count($data['surat']); $i++)
                                <div class="d-flex text-muted pt-3 border-bottom">
                                    <div class="row">
      
                                        <div class="col-md-12 mb-1">
                                          <span style=" font-weight: 600;" >{{ $data['user'][$i]->nama_lengkap }}</span> membuat
                                            <span >{{ $data['surat'][$i]->jenis_surat }} </span>
                                        </div>
                                        <div class="col-md-6 pb-1">
                                          <span class="aktivitas_surat_content">Pada :  {{$data['surat'][$i]->created_at }}</span>
                                        </div>
                                        <div class="col-md-6 pb-1">
                                          <span class="aktivitas_surat_content">{{ $data['surat'][$i]->status }}</span>
                                        </div>
                                    </div>
                                </div>
                            @endfor
                        </div>
                    </div> --}}

                    <div class="col-md-12 pb-5 mb-5">
                      <div class="my-3 p-3 bg-primary rounded shadow-sm">
                          <h6 class="border-bottom text-white pb-2 mb-0">Aktifitas User Terbaru</h6>
                          @for ($i = 0; $i < count($data['user_aktivity']); $i++)
                              <div class="d-flex text-white pt-3 border-bottom">
                                  {{-- <svg class="bd-placeholder-img flex-shrink-0 me-2 rounded" width="32" height="32" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: 32x32" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#007bff"/><text x="50%" y="50%" fill="#007bff" dy=".3em">32x32</text></svg> --}}
                                  <div class="row">
                                      <div class="col-md-12 mb-1">
                                          <span style=" font-weight: 600;" >{{ $data['user_aktivity'][$i]->name }}</span> baru saja terdaftar sebagai
                                          <span >{{ $data['user_aktivity'][$i]->roles }}</span>
                                      </div>
                                      <div class="col-md-6 pb-1 ">
                                        <span class="aktivitas_surat_content"> Pada : {{$data['user_aktivity'][$i]->created_at }}</span>
                                      </div>
                                  </div>
                              </div>
                          @endfor
                      </div>
                    </div>
                  </div>

                </div>
            </main>
        </div>
    </div>

    <div class="footer_user_panel">
        <p><i class="far fa-copyright" style="margin-right: 5px"></i>PESDES - Pengajuan Surat Desa - Gayu Gumelar</p>
    </div>
    @include('Admin.Layout.footer')

    <script src="{{ url('assets/js') }}/admin/dashboard.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js"
        integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"
        integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha" crossorigin="anonymous">
    </script>
