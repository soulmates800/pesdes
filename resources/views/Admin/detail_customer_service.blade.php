@include('Admin.Layout.head')

<body style="background: #F1F3FF">
    <div class="container-fluid">
        <div class="row">
            @include('Admin.Layout.sidebar')
            <main class="col-md-12 ms-sm-auto col-lg-10 px-md-4">
                <div class="row">
                    <div class="col-md-12 bg-white rounded shadow-sm mt-4" >
                        <div class="row mt-5 p-3" id="generateChat" style="height: 400px; overflow: scroll;">
                            
                        </div>
                        <div class="row p-3 mt-3 mb-3">
                            <div class="col-md-10 pb-2">
                                <input type="text" class="form-control" id="message" placeholder="Tulis pesan disini ...">
                              </div>
                              <div class="col-md-2">
                                <button id="buttonKirim" type="button" onclick="kirimPesanChat()" style="width: 100%;" class="btn btn-primary mb-3">Kirim </button>
                              </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>

    @include('Admin.Layout.footer')
    <script src="{{ url('assets/js') }}/admin/detail_customer_service.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>