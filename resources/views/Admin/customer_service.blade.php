@include('Admin.Layout.head')
<body style="background: #F1F3FF">
    <div class="container-fluid">
        <div class="row">
            @include('Admin.Layout.sidebar')
            <main class="col-md-12 ms-sm-auto col-lg-10 px-md-4">
                <div class="row">
                    <div class="col-md-12 bg-white rounded shadow-sm mt-4">
                        <div class="table-responsive">
                            <table style="" id="tables" class="table table-striped border" cellspacing="0" width="100%">
                              <thead style="background: #4764E6; margin-top: 210px; color: white;">
                                <tr>
                                  <th>#</th>
                                  <th>Nama</th>
                                  <th >Kode</th>
                                  <th>Status</th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody id="generateData">

                              </tbody>
                            </table>
                          </div>
                    </div>
                </div>
                <div class="footer_user_panel">
                    <p><i class="far fa-copyright" style="margin-right: 5px"></i>PESDES - Pengajuan Surat Desa - Gayu Gumelar</p>
                </div>
            </main>
        </div>
    </div>

    @include('Admin.Layout.footer')
    <script src="{{ url('assets/js') }}/admin/customer_service.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>