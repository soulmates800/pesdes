@include('Admin.Layout.head')
<link href="https://unpkg.com/filepond@^4/dist/filepond.css" rel="stylesheet" />
<link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css" rel="stylesheet">
<body style="background: #F1F3FF">
    <div class="container-fluid">
        <div class="row">
            @include('Admin.Layout.sidebar')
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="row bg-white rounded shadow-sm mt-4">
                    <div class="col-md-12 mt-3 mb-5">
                      <form id="addUser">
                        {{ csrf_field() }}
                        <div class="row m-l-0 m-r-0">
                        <div class="col-sm-6">
                            <div class="card-block">
                                <label for="inputEmail4" class="form-label">Nama</label>
                                <input type="text" name="name" class="form-control" id="name">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card-block">
                                <label for="inputEmail4" class="form-label">Email</label>
                                <input type="email" name="email" class="form-control" id="email">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card-block">
                                <label for="inputEmail4" class="form-label">Nomor Telepon</label>
                                <input type="text" name="telepon" class="form-control" id="telepon">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card-block">
                                <label for="inputEmail4" class="form-label">Password</label>
                                <input type="password" name="password" class="form-control" id="password">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="card-block">
                                <label for="inputEmail4" class="form-label">Alamat</label>
                                <input type="text" name="alamat" class="form-control" id="alamat">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card-block">
                                <label for="inputEmail4" class="form-label">Agama</label>
                                <input type="text" name="agama" class="form-control" id="agama">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="card-block">
                                <label for="inputEmail4" class="form-label">Jenis Kelamin</label>
                                <select name="jenis_kelamin" class="form-select" id="jenis_kelamin">
                                  <option selected disabled>Pilih Jenis Kelamin</option>
                                  <option value="Laki Laki">Laki Laki</option>
                                  <option value="Perempuan">Perempuan</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="card-block">
                                <label for="inputEmail4" class="form-label">Tempat / Tanggal Lahir</label>
                                <input type="text" name="tempat_tanggal_lahir" class="form-control" id="tempat_tanggal_lahir">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card-block">
                                <label for="inputEmail4" class="form-label">NIK</label>
                                <input type="text" name="nik" class="form-control" id="nik">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card-block">
                                <label for="inputEmail4" class="form-label">No KTP</label>
                                <input type="text" name="no_ktp" class="form-control" id="no_ktp">
                            </div>
                        </div>

                        <div class="col-sm-12">
                          <div class="card-block">
                              <label for="inputEmail4" class="form-label">Status</label>
                              <select name="roles" class="form-select" id="roles">
                                <option selected disabled>Pilih Status</option>
                                <option value="admin">Admin</option>
                                <option value="user">Masyarakat</option>
                              </select>
                          </div>
                      </div>

                        <div class="col-sm-12">
                            <div class="card-block">
                              <label for="inputEmail4" class="form-label">Foto Profile</label>
                                <input type="file" class="my-pond" id="image-tickets" accepted-file-types="image/jpeg, image/png" dropValidation="true" oninput="readURL(this)" storeAsFile="true" data-allow-reorder="true" data-max-file-size="3MB" data-max-files="1">
                            </div>
                        </div>
                            <div class="col-md-12">
                                <div class="card-block">
                                    <button type="submit" class="btn btn-primary">Ubah Profile</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>

                <div class="footer_user_panel">
                    <p><i class="far fa-copyright" style="margin-right: 5px"></i>PESDES - Pengajuan Surat Desa - Gayu
                        Gumelar</p>
                </div>

            </main>
        </div>
    </div>

    @include('Admin.Layout.footer')
    <script src="{{ url('assets/js') }}/admin/adduser.js"></script>