@include('Admin.Layout.head')

<link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet">

<body style="background: #F1F3FF">
  <div class="container-fluid">
    <div class="row">
      @include('Admin.Layout.sidebar')
      <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
        <div class="card mt-3">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12 mt-3">
                <div class="btn-group" role="group" aria-label="Basic outlined example">
                  <button id="btns_semua" type="button" onclick="getSurat('semua')"
                    class="btn btn-outline-primary">Semua</button>
                  <button id="btns_diproses" type="button" onclick="getSurat('Diproses')"
                    class="btn btn-outline-primary">Belum Diproses</button>
                  <button id="btns_diterima" type="button" onclick="getSurat('Diterima')"
                    class="btn btn-outline-primary">Diterima</button>
                  <button id="btns_ditolak" type="button" onclick="getSurat('Ditolak')"
                    class="btn btn-outline-primary">Ditolak</button>
                </div>
              </div>
            </div>
            <div class="row mt-3">
              <div class="col-xs-12">
                <div class="table-responsive">
                  <table class="table table-bordered table-hover dt-responsive" style="font-size: 12px;">
                    <thead>
                      <tr>
                        <th class="text-center">#</th>
                        <th>Jenis Surat</th>
                        <th>Pengirim</th>
                        <th class="text-center">Tanggal</th>
                        <th class="text-center">Status</th>
                        <th class="text-center"></th>
                        <th class="text-center"></th>
                      </tr>
                    </thead>
                    <tbody id="generateData">
  
                    </tbody>
                  </table>
                </div>
                
              </div>
            </div>
          </div>
        </div>
        <div class="footer_user_panel">
          <p><i class="far fa-copyright" style="margin-right: 5px"></i>PESDES - Pengajuan Surat Desa - Gayu
            Gumelar</p>
        </div>
      </main>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal bottom fade" id="bottom_modal" tabindex="-1" role="dialog" aria-labelledby="bottom_modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background: #4764E6;">
          <h5 class="modal-title" id="mdl_status">Bottom Modal Title</h5>
          <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="row g-3 center_costum" id="generateDatas">

            </div>
            <div class="row g-3">
              <div class="col-md-12 pb-3 border_bottom">
                <label class="form-label">Kode Surat</label>
                <input type="text" class="form-control" id="mdls_kodeSurat" disabled>
              </div>
              <div class="col-md-12 pb-3 border_bottom">
                <label class="form-label">Nama Lengkap</label>
                <input type="text" class="form-control" id="mdl_namaLengkap" required disabled>
              </div>
              <div class="col-md-12 text-center pb-3 border_bottom">
                <a id="mdl_link_foto_profile" href="" data-lightbox="image-1" data-title="Foto Profile">
                    <img id="mdl_foto_profile" width="150" height="150" class="imageFcp" class="" src=""/>
                </a>
              </div>
              <div class="col-md-6 pb-3 border_bottom">
                <label class="form-label">Alamat</label>
                <input type="text" class="form-control" id="mdl_alamat" required disabled>
              </div>
              <div class="col-md-6 pb-3 border_bottom">
                <label class="form-label">Email</label>
                <input type="text" class="form-control" id="mdl_email" required disabled>
              </div>
              <div class="col-md-6 pb-3 border_bottom">
                <label class="form-label">Nomor Telepon</label>
                <input type="text" class="form-control" id="mdl_telepon" required disabled>
              </div>
              <div class="col-md-6 pb-3 border_bottom">
                <label class="form-label">Agama</label>
                <input type="text" class="form-control" id="mdl_agama" required disabled>
              </div>
              <div class="col-md-6 pb-3 border_bottom">
                <label class="form-label">Jenis Kelamin</label>
                <input type="text" class="form-control" id="mdl_jenis_kelamin" required disabled>
              </div>
              <div class="col-md-6 pb-3 border_bottom">
                <label class="form-label">Tempat / Tanggal Lahir</label>
                <input type="text" class="form-control" id="mdl_tempat_tanggal_lahir" required disabled>
              </div>
              <div class="col-md-6 pb-3 border_bottom">
                <label class="form-label">NIK</label>
                <input type="text" class="form-control" id="mdl_nik" required disabled>
              </div>
              <div class="col-md-6 pb-3 border_bottom">
                <label class="form-label">Nomor KTP</label>
                <input type="text" class="form-control" id="mdl_no_ktp" required disabled>
              </div>
              
              <div class="col-md-12 pb-3 border_bottom">
                <label class="form-label">Jenis Surat</label>
                <input type="text" class="form-control" id="mdl_jenisSurat" required disabled>
              </div>

              <div class="col-md-6 pb-3 border_bottom hidden-input-surat hidden-pendidikan">
                <label class="form-label">Pendidikan	</label>
                <input type="text" class="form-control" required id="mdl_pendidikan" disabled>
              </div>
              <div class="col-md-6 pb-3 border_bottom hidden-input-surat hidden-penghasilan">
                <label class="form-label">Penghasilan</label>
                <input type="text" class="form-control" required id="mdl_penghasilan" disabled>
              </div>
              <div class="col-md-6 pb-3 border_bottom hidden-input-surat hidden-pekerjaan">
                <label class="form-label">Pekerjaan</label>
                <input type="text" class="form-control" required id="mdl_pekerjaan" disabled>
              </div>
              <div class="col-md-6 pb-3 border_bottom hidden-input-surat hidden-status_perkawinan">
                <label class="form-label">Status Perkawinan</label>
                <input type="text" class="form-control" required id="mdl_status_perkawinan" disabled>
              </div>
              <div class="col-md-6 pb-3 border_bottom hidden-input-surat hidden-kewarganegaraan">
                <label class="form-label">Kewarganegaraan</label>
                <input type="text" class="form-control" required id="mdl_kewarganegaraan" disabled>
              </div>
              <div class="col-md-6 pb-3 border_bottom hidden-input-surat hidden-orpol_ormal">
                <label class="form-label">Orpol / Ormal	</label>
                <input type="text" class="form-control" required id="mdl_orpol_ormal" disabled>
              </div>

              <div class="col-12 pb-3 border_bottom">
                <select style="color: white; font-weight: bolder;" id="prosesSubmit" onchange="handleSelect()"
                  class="form-select" aria-label="Default select example">
                  <option selected value="Diproses">Diproses</option>
                  <option value="Diterima">Terima</option>
                  <option value="Ditolak">Tolak</option>
                </select>
              </div>

              <div class="col-md-6 pb-3 border_bottom surat-diterima">
                <label class="form-label">Tanggal Diambil</label>
                <input type="date" class="form-control" required id="mdl_tanggal_diambil">
              </div>
              <div class="col-md-6 pb-3 border_bottom surat-diterima">
                <label class="form-label">Nama Pengambil Surat</label>
                <input type="text" class="form-control" required id="mdl_pengambil">
              </div>
              <div class="col-md-12 pb-3 border_bottom surat-ditolak">
                <div class="form-floating">
                  <textarea class="form-control" rows="20" placeholder="Tuliskan mengapa surat ini ditolak" id="mdl_alasan_ditolak"></textarea>
                  <label for="alasan_ditolak">Alasan Surat Ditolak</label>
                </div>
              </div>
              {{-- <textarea name="" id="" cols="30" rows="10"></textarea> --}}
              <div class="col-12 pb-3 border_bottom">
                <button type="button" class="btn btn-secondary" id="closeBtmModal" data-dismiss="modal">Tutup</button>
                <button type="button" onclick="submitSurat()" class="btn btn-primary">Simpan</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('Admin.Layout.footer')

  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"
    integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ=="
    crossorigin="anonymous"></script>
  <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

  <script src="{{ url('assets/js') }}/admin/surat.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js"
    integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"
    integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha" crossorigin="anonymous">
  </script>