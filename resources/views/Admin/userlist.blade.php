@include('Admin.Layout.head')

<body style="background: #F1F3FF">
    <div class="container-fluid footer-padding">
        
        <div class="row">
            @include('Admin.Layout.sidebar')
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mt-3">
                        <div class="card text-muted">
                            <div class="single-review-st-item res-mg-t-30 table-mg-t-pro-n">
                                <div class="single-review-st-hd mb-5">
                                    <h2>Daftar User</h2>
                                </div>
                                <span style="display: inline !important;" id="generateUser"></span>
                            </div>
                        </div>
                    </div>
                        <div class="col-md-8 mt-3 mb-2">
                            <div class="single-cards-item">
                            <div class="page-content page-container" id="page-content">
                                <div class="row container d-flex justify-content-center">
                                    <div class="row m-l-0 m-r-0">
                                        <div class="col-sm-12 bg-c-lite-green user-profile">
                                            <div class="card-block text-center text-white">
                                                <div class="m-b-25"> <img style="width: 70px; height: 70px" id="pictureUser"
                                                        src="https://img.icons8.com/bubbles/100/000000/user.png"
                                                        class="img-radius" alt="User-Profile-Image"> </div>
                                                <h6 class="f-w-600" id="namaUser"></h6>
                                                <p id="roleUser"></p> <i
                                                    class=" mdi mdi-square-edit-outline feather icon-edit m-t-10 f-16"></i>
                                            </div>
                                        </div>
                                        <div class="col-sm-12" style="overflow: hidden">
                                            <div class="row m-l-0 m-r-0">
                                                <div class="col-sm-6">
                                                    <div class="card-block">
                                                        <p class="f-w-600 text-muted" style="margin-bottom: 3px;">Nama</p>
                                                    <h6 class="f-w-400" id="namaUser2"></h6>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="card-block">
                                                        <p class="f-w-600 text-muted" style="margin-bottom: 3px;">Email</p>
                                                    <h6 class="f-w-400" id="emailUser"></h6>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="card-block">
                                                        <p class="f-w-600 text-muted" style="margin-bottom: 3px;">Nomor Telepon</p>
                                                    <h6 class="f-w-400" id="teleponUser"></h6>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="card-block">
                                                        <p class="f-w-600 text-muted" style="margin-bottom: 3px;">Password</p>
                                                    <h6 class="f-w-400" id="passwordUser"></h6>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="card-block">
                                                        <p class="f-w-600 text-muted" style="margin-bottom: 3px;">Alamat</p>
                                                    <h6 class="f-w-400" id="alamatUser"></h6>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="card-block">
                                                        <p class="f-w-600 text-muted" style="margin-bottom: 3px;">Agama</p>
                                                    <h6 class="f-w-400" id="agamaUser"></h6>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="card-block">
                                                        <p class="f-w-600 text-muted" style="margin-bottom: 3px;">Jenis Kelamin</p>
                                                    <h6 class="f-w-400" id="jeniskelaminUser"></h6>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="card-block">
                                                        <p class="f-w-600 text-muted" style="margin-bottom: 3px;">Tempat / Tanggal Lahir</p>
                                                    <h6 class="f-w-400" id="tanggalahirUser"></h6>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="card-block">
                                                        <p class="f-w-600 text-muted" style="margin-bottom: 3px;">NIK</p>
                                                    <h6 class="f-w-400" id="nikUser"></h6>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="card-block">
                                                        <p class="f-w-600 text-muted" style="margin-bottom: 3px;">No KTP</p>
                                                    <h6 class="f-w-400" id="ktpUser"></h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="single-cards-item mt-3">
                            <form class="row g-3 p-3">
                                <div class="col-md-12">
                                    <span style="font-size: 12px; opacity: 0.7;">Jika user lupa atau kehilangan password. Kamu dapat mengubahnya disini</span>
                                  </div>
                                <div class="col-md-9">
                                  <label for="inputPassword2" class="visually-hidden">Password</label>
                                  <input type="password" class="form-control" name="password" id="placeHold" placeholder="">
                                </div>
                                <div class="col-md-3">
                                  <button type="button" onclick="changePassword()" style="width: 100%;" class="btn btn-primary">Kirimkan</button>
                                </div>
                                <div class="col-md-12">
                                    <button type="button" id="deleteUserButton" onclick="deleteUser()" style="width: 100%;" class="btn btn-danger mb-3"></button>
                                  </div>
                              </form>
                        </div>
                    </div>
                </div>

                <div class="footer_user_panel">
                    <p><i class="far fa-copyright" style="margin-right: 5px"></i>PESDES - Pengajuan Surat Desa - Gayu
                        Gumelar</p>
                </div>

            </main>
        </div>
    </div>

    @include('Admin.Layout.footer')
    <script src="{{ url('assets/js') }}/admin/userlist.js"></script>