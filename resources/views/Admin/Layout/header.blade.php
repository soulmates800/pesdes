<header class="navbar navbar-dark sticky-top flex-md-nowrap shadow blue_nav_costum" style="background: #4764E6;">
    <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#">Pengajuan Surat Desa</a>
    <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    {{-- <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search"> --}}
    <ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
        <a class="nav-link" href="#"><span style="font-size: 12px">{{$data['user_data']->roles}},</span> <b>{{$data['user_data']->name}}</b></a>
    </li>
    </ul>
</header>