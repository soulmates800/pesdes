
<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block sidebar collapse" style="background-color: #fff;">
    <div class="position-sticky pt-3">
      <ul class="nav flex-column">
        <li class="nav-item mt-1 mb-1" style="border-bottom: 1px solid #ebebeb">
          <a class="nav-link" href="{{ url('admin/home') }}">
            <span data-feather="file"></span>
            <i class="fas fa-home" style="margin-right: 10px"></i>Dashboard
          </a>
        </li>
        <li class="nav-item mt-1 mb-1 mt-3" style="border-bottom: 1px solid #ebebeb">
          <a class="nav-link" href="{{ url('admin/surat') }}">
            <span data-feather="file"></span>
            <i class="fas fa-layer-group" style="margin-right: 10px"></i>Daftar Surat
          </a>
        </li>
        <li class="nav-item mt-1 mb-1" style="border-bottom: 1px solid #ebebeb">
          <a class="nav-link" href="{{ url('admin/daftar_user') }}">
            <span data-feather="shopping-cart"></span>
            <i class="fas fa-venus-double" style="margin-right: 10px"></i>Daftar User
          </a>
        </li>
        <li class="nav-item mt-1 mb-1" style="border-bottom: 1px solid #ebebeb">
          <a class="nav-link" href="{{ url('admin/adduser') }}">
            <span data-feather="shopping-cart"></span>
            <i class="fas fa-plus-square" style="margin-right: 10px"></i>Tambah User
          </a>
        </li>
        <li class="nav-item mt-1 mb-1" style="border-bottom: 1px solid #ebebeb">
          <a class="nav-link" href="{{ url('admin/profile') }}">
            <span data-feather="shopping-cart"></span>
            <i class="fas fa-user-circle" style="margin-right: 10px"></i>Profile
          </a>
        </li>
        <li class="nav-item mt-1 mb-1" style="border-bottom: 1px solid #ebebeb">
          <a class="nav-link" href="{{ url('admin/chat') }}">
            <span data-feather="shopping-cart"></span>
            <i class="fab fa-rocketchat" style="margin-right: 10px"></i>Customer Service
          </a>
        </li>
        <form id="logoutUser" method="POST" action="{{ route('logout') }}" >
          @csrf
        <li onclick="event.preventDefault(); this.closest('form').submit();" class="nav-item mt-3" style="border-bottom: 1px solid #ebebeb;">
            <a class="nav-link" href="#">
              <span data-feather="shopping-cart"></span>
              <i class="fas fa-sign-out-alt" style="margin-right: 10px"></i>
             
            Logout
            </a>
          </li>
        </form>
      </ul>

      
    
    </div>
  </nav>