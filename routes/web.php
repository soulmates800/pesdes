<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Frontend as Frontend;
use App\Http\Controllers\MailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Main.landing');
});

Route::get('/tentang', function () {
    return view('Main.tentang');
});

Route::get('/lupa-password', function () {
    return view('Main.lupa_password');
});

Route::get('/test', function () {
    return view('Main.email_terkirim');
});

Route::get('/send-email/{email}', [MailController::class, 'sendEmail']);

Route::get('/register', [Frontend\LoginControllers::class, 'register']);
Route::get('/login', function () {
    return view('Main.login');
})->name('/login');

Route::prefix('user')->group(function () {
    Route::get('/home', [Frontend\HomeControllers::class, 'index']);
    Route::get('/surat', [Frontend\SuratControllers::class, 'index']);
    Route::get('/detail_surat/{kode}', [Frontend\SuratControllers::class, 'detail']);
    Route::get('/update_surat/{id}', [Frontend\SuratControllers::class, 'update_surat']);
    Route::get('/profile', [Frontend\ProfileControllers::class, 'index']);
});

Route::prefix('admin')->group(function () {
    Route::get('/home', [Frontend\HomeControllers::class, 'adm_index']);
    Route::get('/surat', [Frontend\SuratControllers::class, 'adm_index']);
    Route::get('/adduser', [Frontend\UserControllers::class, 'adm_tambah_user']);
    Route::get('/daftar_user', [Frontend\UserControllers::class, 'adm_daftar_user']);
    Route::get('/profile', [Frontend\UserControllers::class, 'adm_profile']);
    Route::get('/chat', [Frontend\ChatControllers::class, 'adm_index']);
    Route::get('/detail_chat/{id_room}', [Frontend\ChatControllers::class, 'adm_detail_chat']);
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');
