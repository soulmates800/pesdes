<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Backend as Backend;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [Backend\LoginControllers::class, 'AuthLogin']);
Route::post('/register', [Backend\LoginControllers::class, 'AuthRegister']);
Route::post('/logout', [Backend\LoginControllers::class, 'AuthLogout'])->middleware('auth')->name('logout');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('user')->group(function () {
    
    Route::post('/kirim_chat', [Backend\ChatControllers::class, 'kirim_chat']);
    Route::get('/get_pesan_chat', [Backend\ChatControllers::class, 'get_pesan_chat']);

    Route::post('/buat_surat', [Backend\SuratControllers::class, 'buat_surat']);
    Route::get('/detail_surat/{kode}', [Backend\SuratControllers::class, 'detail_surat']);
    Route::get('/get_surat', [Backend\SuratControllers::class, 'get_surat']);
    Route::get('/get_info', [Backend\SuratControllers::class, 'get_info']);
    Route::get('/get_notifikasi', [Backend\SuratControllers::class, 'get_notifikasi']);
    Route::get('/notifikasi/{kode}', [Backend\SuratControllers::class, 'update_notifikasi']);
    Route::post('/update_surat', [Backend\SuratControllers::class, 'update_surat']);

});

Route::prefix('admin')->group(function () {
    Route::get('/get_surat/{params}', [Backend\SuratControllers::class, 'adm_get_surat']);
    Route::post('/proses_surat', [Backend\SuratControllers::class, 'proses_surat']);
    Route::get('/daftar_user', [Backend\UserControllers::class, 'daftar_user']);
    Route::post('/add_user', [Backend\UserControllers::class, 'add_user']);
    Route::get('/profile', [Backend\ProfileControllers::class, 'profile']);
    Route::post('/updateUser', [Backend\ProfileControllers::class, 'updateUser']);
    Route::post('/update_list_user', [Backend\UserControllers::class, 'update_list_user']);
    Route::post('/delete_user', [Backend\UserControllers::class, 'delete_user']);
    Route::get('/get_room_chat', [Backend\ChatControllers::class, 'adm_get_room_chat']);
    Route::get('/adm_get_chat/{room_chat}', [Backend\ChatControllers::class, 'adm_get_chat']);
    Route::post('/adm_kirim_chat', [Backend\ChatControllers::class, 'adm_kirim_chat']);
    Route::get('/delete_surat/{id}', [Backend\SuratControllers::class, 'delete_surat']);
});