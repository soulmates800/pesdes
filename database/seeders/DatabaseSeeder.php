<?php

namespace Database\Seeders;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Yovie Alfa Guistuta',
            'email' => 'yoviealfa@gmail.com',
            'telepon' => '0895636134374',
            'password' => Hash::make('admin'),
            'roles' => 'user',
            'email_verified_at' => Carbon::now(),
            'remember_token' => Str::random(100),
        ]);

        DB::table('users')->insert([
            'name' => 'Gayu Gumelar',
            'email' => 'gayu@gmail.com',
            'telepon' => '0895636134374',
            'password' => Hash::make('admin'),
            'roles' => 'admin',
            'email_verified_at' => Carbon::now(),
            'remember_token' => Str::random(100),
        ]);

    }
}
